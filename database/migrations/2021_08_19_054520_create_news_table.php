<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->required();
            $table->string('title')->required();
            $table->string('slug')->required();
            $table->string('image')->nullable()->default('default.jpg');
            $table->longText('summary')->required();
            $table->string('tags')->nullable();
            $table->string('date_created')->required();
            $table->unsignedBigInteger('created_by')->required();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->longText('content')->required();
            $table->tinyInteger('is_deleted')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
