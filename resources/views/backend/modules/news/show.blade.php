@extends('backend.layouts.app')

@section('title', __('News Article'))

@section('content')
    <div class="row">
        <div class="col-md-8">
            <x-backend.card>
                <x-slot name="header">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00" for="news">Title</label>
                            <input value="{{$news->title}}" name="title" id="news_title" type="text" placeholder="{{$news->title}}" onload="slugify(this.value)" onkeyup="slugify(this.value)" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                        </div>
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00" for="news">Slug</label>
                            <input value="{{$news->slug}}" name="slug" id="slug" type="text" placeholder="{{$news->slug}}" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                        </div>
                    </div>
                </x-slot>
                <x-slot name="body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Summary</label>
                            <textarea name="summary" id="" cols="30" rows="3" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                            >{{$news->summary}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news"">Content</label>
                            <textarea name="content" id="content" cols="30" rows="10" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                            >{!! $news->content !!}</textarea>
                        </div>
                    </div>
                </x-slot>
            </x-backend.card>
        </div>
        <div class="col-md-4">
            <x-backend.card>
                <x-slot name="header">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="/uploads/news/<?php echo $news->image ?>" alt="" width="150px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Image</label>
                            <input value="{{$news->image}}" type="file" name="image" id="">
                        </div>
                    </div>
                </x-slot>
                <x-slot name="body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Date</label>
                            <input value="{{$news->date_created}}" type="date" name="date_created" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Author</label>
                            <input name="created_by" type="hidden" value="{{auth()->user()->id}}">
                            <input type="text" placeholder="Author" readonly value="{{auth()->user()->name}}"
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Category</label>
                            <select value="{{$news->category_id}}" name="category_id" id="" readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                                <option value="1">School-Related</option>
                                <option value="2">School-test</option>
                                <option value="3">School-unrelated</option>
                                <option value="4">School-bleh</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block mt-2 text-sm text-gray-00" for="news">Tags</label>
                            <input value="{{$news->tags}} "type="text" name="tags"  data-separator=" " readonly
                                class="w-full px-2 py-2 text-black border border-transparent rounded tagin focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                            <small>NOTE: Separate each keyword with a space</small>
                        </div>
                    </div>
                </x-slot>
            </x-backend.card>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //Tiny WYSIWYG Editor
        tinymce.init({
            selector: '#content'
        })
        //Tags
        tagin( document.querySelector('.tagin') )
        //Text-to-slug
        function slugify(title) {
            //replace all special characters | symbols with a space
            title = title.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();

            // trim spaces at start and end of string
            title = title.replace(/^\s+|\s+$/gm,'');

            // replace space with dash/hyphen
            title = title.replace(/\s+/g, '-');

            document.getElementById('slug').value = title
        }
    </script>
@endsection
