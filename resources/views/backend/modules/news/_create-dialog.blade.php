<div
    class="modal fade"
    role="dialog"
    tabindex="-1"
    id="create-news-modal"
    aria-labelledby="createNewsDialog"
    aria-hidden="true"
    data-keyboard="false"
    data-backdrop="static"
>
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{route('admin.news.store')}}" id="submit" method="post" enctype="multipart/form-data" novalidate>
        @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add News Entry</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00" for="news">Title</label>
                            <input name="title" id="news_title" type="text" placeholder="News Title" onload="slugify(this.value)" onkeyup="slugify(this.value)" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                        </div>
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00" for="news">Slug</label>
                            <input name="slug" id="slug" type="text" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Category</label>
                            <select name="category_id" id="" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                                <option value="1" selected>School-Related</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Tags</label>
                            <input type="text" name="tags"  data-separator=" " required
                                class="tagin w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                            <small>NOTE: Separate each keyword with a space</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Date</label>
                            <input type="date" name="date_created" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                        </div>
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Author</label>
                            <input name="created_by" type="hidden" value="{{auth()->user()->id}}">
                            <input type="text" placeholder="Author" readonly value="{{auth()->user()->name}}"
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Image</label>
                            <input type="file" name="image" id="" required>
                        </div>
                        <div class="col-md-6">
                            <label class="block text-sm text-gray-00 mt-2" for="news">Summary</label>
                            <textarea name="summary" id="" cols="30" rows="3" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded"
                            ></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block text-sm text-gray-00 mt-2" for="news"">Content</label>
                            <textarea name="content" id="content" cols="30" rows="10" required
                                class="w-full text-black px-2 py-2 border border-transparent focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent rounded"
                            ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                        class="bg-white text-gray-800 font-bold rounded border-b-2 border-green-500 hover:border-green-600 hover:bg-green-500 shadow-md py-2 px-6 inline-flex items-center"
                    >
                        <span class="mr-2">Submit</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
    .input {
        transition: border 0.2s ease-in-out;
        min-width: 280px
    }

    .input:focus+.label,
    .input:active+.label,
    .input.filled+.label {
        font-size: .75rem;
        transition: all 0.2s ease-out;
        top: -0.1rem;
        color: #667eea;
    }

    .label {
        transition: all 0.2s ease-out;
        top: 0.4rem;
      	left: 0;
    }
</style>
<script>
    var toggleInputContainer = function (input) {
        if (input.value != "") {
            input.classList.add('filled');
        } else {
            input.classList.remove('filled');
        }
    }

    var labels = document.querySelectorAll('.label');
    for (var i = 0; i < labels.length; i++) {
        labels[i].addEventListener('click', function () {
            this.previousElementSibling.focus();
        });
    }

    window.addEventListener("load", function () {
        var inputs = document.getElementsByClassName("input");
        for (var i = 0; i < inputs.length; i++) {
            console.log('looped');
            inputs[i].addEventListener('keyup', function () {
                toggleInputContainer(this);
            });
            toggleInputContainer(inputs[i]);
        }
    });

    //Third Party
    tinymce.init({
        selector: '#content'
    })
    tagin( document.querySelector('.tagin') );

    // Text to slug
    function slugify(title) {
        //replace all special characters | symbols with a space
        title = title.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();

        // trim spaces at start and end of string
        title = title.replace(/^\s+|\s+$/gm,'');

        // replace space with dash/hyphen
        title = title.replace(/\s+/g, '-');

        document.getElementById('slug').value = title
    }
</script>
