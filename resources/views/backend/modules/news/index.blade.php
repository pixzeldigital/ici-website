@extends('backend.layouts.app')

@section('title', __('News'))

@section('content')
    <div>
        @include('backend.modules.news._create-dialog')
        <x-backend.card>
            <x-slot name="header">
                <div class="flex justify-between">
                    <div class="flex p-2 space-x-4 bg-gray-100 rounded-lg w-72">
                        <form action="{{route('admin.news.index')}}" method="get">
                            <input class="pl-2 text-black bg-gray-100 outline-none" type="text" name="keyword" placeholder="Search for news" />
                        </form>
                    </div>
                    <div class="">
                        <button
                            data-toggle="modal"
                            data-target="#create-news-modal"
                            class="inline-flex items-center px-6 py-2 font-bold text-gray-800 bg-white border-b-2 border-green-500 rounded shadow-md hover:border-green-600 hover:bg-green-500"
                        >
                        <span class="mr-2">Add News</span>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                        </svg>
                        </button>
                    </div>
                </div>
            </x-slot>

            <x-slot name="body">
                <table class="min-w-max">
                    <thead>
                        <tr class="text-sm leading-normal text-gray-600 uppercase bg-gray-200">
                            <th class="px-3 py-1 text-left" width="1px">#</th>
                            <th class="px-6 py-3 text-left" width="50px">Title</th>
                            <th class="px-6 py-3 text-left" width="100px">Summary</th>
                            <th class="px-6 py-3 text-center" width="10px">Date</th>
                            <th class="px-6 py-3 text-center" width="10px">Author</th>
                            <th class="px-6 py-3 text-center" width="1%">Category</th>
                            <th class="px-6 py-3 text-center" width="1%">Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-sm font-light text-gray-600">
                        @foreach ($news as $i => $news_item )
                        <tr class="border-b border-gray-200 bg-gray-50 hover:bg-gray-100">
                            <td class="px-3 py-1 text-left" width="1px">
                                <div class="flex items-left">
                                    <span class="font-medium">{{++$i}}</span>
                                </div>
                            </td>
                            <td class="px-3 py-1 text-left" width="50px">
                                <div class="flex items-center">
                                    <span class="font-medium">{{$news_item->title}}</span>
                                </div>
                            </td>
                            <td class="px-3 py-1 text-left" width="100px">
                                <div class="flex items-center">
                                    <span class="font-medium">{{$news_item->summary}}</span>
                                </div>
                            </td>
                            <td class="px-3 py-1 text-center" width="1%">
                                <div class="">
                                    <span class="font-medium">{{$news_item->date_created}}</span>
                                </div>
                            </td>
                            <td class="px-3 py-1 text-center" width="1%">
                                <div class="">
                                    <span>{{$news_item->author}}</span>
                                </div>
                            </td>
                            <td class="px-3 py-1 text-center" width="1%">
                                <span class="px-3 py-1 text-xs text-green-600 bg-green-200 rounded-full">Completed</span>
                            </td>
                            <td class="px-3 py-1 text-center" width="10%">
                                <form action="{{route('admin.news.destroy', $news_item->id)}}" method="post">
                                    <div class="flex justify-center item-center">
                                        <a href="{{route('admin.news.show', $news_item->id)}}">
                                            <div class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                </svg>
                                            </div>
                                        </a>
                                        <a href="{{route('admin.news.edit', $news_item->id)}}">
                                            <div class="w-4 mr-2 transform hover:text-green-500 hover:scale-110">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                </svg>
                                            </div>
                                        </a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('Are you sure you want to delete?');">
                                            <div class="w-4 mr-2 transform hover:text-red-500 hover:scale-110">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                </svg>
                                            </div>
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </x-slot>
            <x-slot name="footer">
                <div class="flex justify-end">
                    <div>
                        {{$news->links()}}
                    </div>
                </div>
            </x-slot>
        </x-backend.card>
    </div>
@endsection
