<div
    class="modal fade"
    role="dialog"
    tabindex="-1"
    id="create-category-modal"
    aria-labelledby="createCategoryDialog"
    aria-hidden="true"
    data-keyboard="false"
    data-backdrop="static"
>
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{route('admin.categories.store')}}" id="submit" method="post" enctype="multipart/form-data" novalidate>
        @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Category</h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-4 row">
                        <div class="col-md-12">
                            <label class="block text-sm text-gray-00" for="news">Title</label>
                            <input name="name" id="category_name" type="text" placeholder="Category name..." required
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="block text-sm text-gray-00" for="news">Description</label>
                            <textarea name="description" id="" cols="30" rows="3" required
                                class="w-full px-2 py-2 text-black border border-transparent rounded focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                            ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                        class="inline-flex items-center px-6 py-2 font-bold text-gray-800 bg-white border-b-2 border-green-500 rounded shadow-md hover:border-green-600 hover:bg-green-500"
                    >
                        <span class="mr-2">Submit</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
