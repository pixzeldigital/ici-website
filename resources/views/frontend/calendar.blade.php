@extends('frontend.layouts.app')

@section('title', __('Careers'))

@push('after-styles')
<link href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/careers-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Calendar</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h6 class="mb-3">Categories</h6>

				<input id="1" class="event_filter_box" name="event_filter_select" type="checkbox" value="1" data-type="audience">Parents</input>

<input id="2" class="event_filter_box" name="event_filter_select" type="checkbox" value="2" data-type="audience">Students</input>

<input id="hamburg" class="event_filter_box" name="event_filter_select" type="checkbox" value="Hamburg" data-type="address_city">Hamburg</input>
<input id="schleswig-holstein" class="event_filter_box" name="event_filter_select" type="checkbox" value="Schleswig-Holstein" data-type="address_city">Schleswig-Holstein</input>

			</div>
			<div class="col-md-9">
				<div id="calendar"></div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('after-scripts')
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js"></script>
<script>



  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      textColor: '#ccc',
      events: [
        {
            title: 'Start of Parents’ orientation batch 1 to 6',
            start: '2021-08-02',
            end: '2021-08-07',
            audience: '1',
            branch: 'iligan',
            backgroundColor: '#28ACB5',
            borderColor: '#28ACB5',
        },
        {
            title: 'Receiving of modules',
            start: '2021-08-02',
            end: '2021-08-07',
            audience: '1',
            branch: 'iligan',
            backgroundColor: '#28ACB5',
            borderColor: '#28ACB5',
        },
        {
            title: 'Start of  Learning activities',
            start: '2021-08-09',
            audience: '2',
            branch: 'iligan',
            backgroundColor: '#28ACB5',
            borderColor: '#28ACB5',
        },
        {
            title: 'Observance of Buwan ng Wika',
            start: '2021-08-23',
            end: '2021-08-31',
            audience: '2',
            branch: 'iligan',
            backgroundColor: '#28ACB5',
            borderColor: '#28ACB5',
        },
        {
            title: 'Applicaton for Candidacy',
            start: '2021-08-30',
            audience: '2',
            branch: 'iligan',
            classNames: "event-ilg"
        },
        {
            title: 'Briefing of teachers for Parents Orientation',
            start: '2021-08-02',
            audience: '3',
            branch: 'iligan',
            classNames: "event-ilg"
        },
    ],
    eventDidMount: function(info) {
	  var display = true;
	      var audiences = [];
	      var branches = [];
	      // Find all checkbox that are event filters that are enabled
	      // and save the values.
	      $("input[name='event_filter_select']:checked").each(function () {
	          // I specified data-type attribute in above HTML to differentiate
	          // between locations and kinds of events.
	          
	          // Saving each type separately
	          if ($(this).data('type') == 'audience') {
	              audiences.push($(this).val());
	          }
	          else if ($(this).data('type') == 'branch') {
	              branches.push($(this).val());
	          }
	      });

	      // If there are locations to check
	      if (audiences.length) {
	          display = display && audiences.indexOf(event.audience) >= 0;
	      }

	      // If there are specific types of events
	      if (branches.length) {
	          display = display && branches.indexOf(event.branch) >= 0;
	      }

	      return display;
	  },
	},
    eventRender: function( event, element, view ) {
        var display = true;
	      var audiences = [];
	      var branches = [];
	      // Find all checkbox that are event filters that are enabled
	      // and save the values.
	      $("input[name='event_filter_select']:checked").each(function () {
	          // I specified data-type attribute in above HTML to differentiate
	          // between locations and kinds of events.
	          
	          // Saving each type separately
	          if ($(this).data('type') == 'audience') {
	              audiences.push($(this).val());
	          }
	          else if ($(this).data('type') == 'branch') {
	              branches.push($(this).val());
	          }
	      });

	      // If there are locations to check
	      if (audiences.length) {
	          display = display && audiences.indexOf(event.audience) >= 0;
	      }

	      // If there are specific types of events
	      if (branches.length) {
	          display = display && branches.indexOf(event.branch) >= 0;
	      }

	      return display;
	  },
    });

  
    calendar.render();

    $('input[class=event_filter_box]').change(function() {
	     calendar.refetchEvents();
	  });
  });


</script>
@endpush
