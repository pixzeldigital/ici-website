@extends('frontend.layouts.app')

@section('title', __('Home'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')

<section class="bg-secondary height-sm-60 pt-0 pb-0">

        <div class="row">
        <div class="col-lg-5 col-sm-12">
          <div class="home-text">
              <h1 class="pb-4">The <span class="c-magenta">Most Trusted</span><br>Senior High School<br>in Northern Mindanao</h1>

            <p class="pb-4">ICI stays true to its mission of producing competent graduates for the workforce by providing relevant academic programs and industry standard facilities for its students.</p>

            <div class="hidden-xs">
              <a class="btn btn-primary" href="/education">Our Offerings</a> 
              <a class="btn" href="/admissions">Enroll Now</a>
            </div>

            <div class="hidden-lg hidden-md hidden-sm">
              <a class="btn btn-primary btn-block" href="/education">Our Offerings</a> 
              <a class="btn btn-block mt-2" href="/admissions">Enroll Now</a>
            </div>
          </div>
            
        </div>
        <div class="col-lg-7 col-sm-12 pr-0 pl-xs-0 pl-sm-0 hidden">
           <div id="carouselExampleIndicators" class="carousel slide slider-carousel" data-ride="carousel" data-interval="5000" data-touch="true">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="/img/hero-1.jpg" class="d-block w-100" alt="#AsaNaTaBai">
                </div>
                <div class="carousel-item">
                  <img src="/img/hero-2.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                  <img src="/img/hero-3.jpg" class="d-block w-100" alt="...">
                </div>
              </div>
            </div>
       
        </div>
    </div>
</section>

<!-- <section class="space--xs">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <h4>Announcements</h4>
                    <a href="" class="c-magenta">Read all Announcements &nbsp; <i class="fas fa-arrow-right"></i></a>
                </div>
                
                <hr class="mt-1 mb-4">

                <div class="row justify-content-between">
                    <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/a-1.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>Career Guidance for Grade 12: Interview Preparation Tips for graduating learners</h6>
                            <p class="card-text">After watching the topic talk, please fill out the evaluation form given below. https://docs.google.com/...</p>
                          </div>
                          </a>
                        </div>
                    </div>

                    <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/a-2.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>Grade 12: Exit Interview after graduation pictorial</h6>
                            <p class="card-text">Heads up G12 students, The School Guidance would like to inform you that we will be having an exit interview starting tomorrow, March 18, 2021. </p>
                          </div>
                          </a>
                        </div>
                    </div>

                     <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/a-3.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>Grade 11: An ICI-Iligan Guidance Webinar</h6>
                            <p class="card-text">After watching the topic talk, please fill out the evaluation form given below. </p>
                          </div>
                          </a>
                        </div>
                    </div>
                </div>

                 <div class="d-flex justify-content-between mt-5">
                    <h4>ICI News</h4>
                    <a href="" class="c-magenta">Read all News &nbsp; <i class="fas fa-arrow-right"></i></a>
                </div>
                <hr class="mt-1 mb-4">

                <div class="row justify-content-between">
                    <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/n-1.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>ICI is now included in the UP College Admissions Online Application Portal! </h6>
                            <p class="card-text">Grade 12 ICIans: Our school is now included in the UP College Admissions Online Application Portal! </p>
                          </div>
                          </a>
                        </div>
                    </div>

                    <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/n-2.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>GICI Weekly CASH Pahalipay for all Grade 10 Students in Iligan City</h6>
                            <p class="card-text">Win up to P1,000 Cash! Participants MUST be a GRADE 10 STUDENT in any PUBLIC or PRIVATE school in Iligan City.</p>
                          </div>
                          </a>
                        </div>
                    </div>

                     <div class="col-md-4 pl-0 first">
                        <div class="card no-border">
                          <a href="">
                          <img src="img/n-3.jpg" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h6>1st Batch of Parent-Teacher Conference (Virtual & Limited). </h6>
                            <p class="card-text">Thank you dear parents for your time, rest assured all your concerns will be addressed promptly.</p>
                          </div>
                          </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <img src="img/sidebar.png">
            </div>

        </div>
    </div>
</section> -->

<section class="space--xs">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
                <div id="about-ici" class="carousel slide carousel-fade first align-content-center" data-ride="carousel" data-touch="true">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="img/about-ici-2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                      <img src="img/about-ici-3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                      <img src="img/about-ici.jpg" class="d-block w-100" alt="...">
                    </div>
                  </div>
                
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card card-about second">
                    <div class="card-body c-white p-5">
                    <h2 class="c-white mb-3">Welcome to ICI</h2>
                    <p class="card-text">ICI is one of the leading providers of specialized upper secondary and technical education in Iligan City and its neighboring towns. </p>
                    <p class="card-text">Our hands-on approach to education gives our students an edge in the world of employment. ICI boasts of industry standard facilities and equipment in each of its branches. As a result, our graduates are well-experienced individuals ready for life beyond our classrooms. </p>
                    <a href="/about" class="">More About Us &nbsp; <i class="fas fa-arrow-right"></i></a>
                  </div>
                </div>
            </div>
        </div>
        
    </div>
    
</section>

<section class="bg-secondary space--xs">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
               <!--  <h2 class="first">ICI in Figures</h2> -->
                <!-- <p class="mb-5 first">ICI boasts of industry standard facilities and equipment in each of its branches. The school is committed to giving its students a hands-on approach to provide them an edge in the world of employment. ICI graduates are productive, self-directed, and well-experienced individuals.</p> -->

                <div class="row justify-content-around">
                    <div class="grid">
                      <div class="col-md-3 first">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="150" height="150" xmlns="http://www.w3.org/2000/svg">
                          <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <circle class="circle-chart__circle" stroke="#28ACB5" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <g class="circle-chart__info">
                            <text class="circle-chart__percent font-bold" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="10">3</text>
                            <text class="circle-chart__subline" x="16.91549431" y="21.5" alignment-baseline="central" text-anchor="middle" font-size="3">Locations</text>
                          </g>
                        </svg>
                      </div>

                        <div class="col-md-3 second">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="150" height="150" xmlns="http://www.w3.org/2000/svg">
                          <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <circle class="circle-chart__circle" stroke="#198DC6" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <g class="circle-chart__info">
                            <text class="circle-chart__percent font-bold" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="10">24</text>
                            <text class="circle-chart__subline" x="16.91549431" y="21.5" alignment-baseline="central" text-anchor="middle" font-size="3">Years in Business</text>
                          </g>
                        </svg>
                      </div>

                         <div class="col-md-3 third">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="150" height="150" xmlns="http://www.w3.org/2000/svg">
                          <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <circle class="circle-chart__circle" stroke="#D72958" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <g class="circle-chart__info">
                            <text class="circle-chart__percent font-bold" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="10">100+</text>
                            <text class="circle-chart__subline" x="16.91549431" y="21.5" alignment-baseline="central" text-anchor="middle" font-size="3">Faculty & Staff</text>
                          </g>
                        </svg>
                      </div>

                         <div class="col-md-3 fourth">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="150" height="150" xmlns="http://www.w3.org/2000/svg">
                          <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <circle class="circle-chart__circle" stroke="#9c9c9c" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                          <g class="circle-chart__info">
                            <text class="circle-chart__percent font-bold" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="10">5k+</text>
                            <text class="circle-chart__subline" x="16.91549431" y="21.5" alignment-baseline="central" text-anchor="middle" font-size="3">SHS Population</text>
                          </g>
                        </svg>
                      </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<section class="space--xs">
  <div class="container">
     <div class="row text-center">
            <div class="col-md-12">
              <h1 class="c-primary">Enrollment is still ongoing until September 2, 2021!</h1>
              <p class=" mb-3">Click on your desired campus and fill up the Branch Enrollment Form.</p>

              <div class="hidden-xs">
                <a class="btn btn-lg mt-3 mr-2" href="http://bit.ly/ICIILIGANOnlineReg2021" target="_blank">Iligan</a> 
                <a class="btn btn-lg mt-3 mr-2" href="http://bit.ly/ICICDOOnlineReg2021" target="_blank">Cagayan de Oro</a>
                <a class="btn btn-lg mt-3" href="http://bit.ly/ICIKapataganSHSEnrollmentForm" target="_blank">Kapatagan</a>
              </div>

              <div class="hidden-lg hidden-md hidden-sm">
                 <a class="btn btn-lg btn-block" href="http://bit.ly/ICIILIGANOnlineReg2021" target="_blank">Iligan</a> 
                <a class="btn btn-lg mt-2 btn-block" href="http://bit.ly/ICICDOOnlineReg2021" target="_blank">Cagayan de Oro</a>
                <a class="btn btn-lg mt-2 btn-block" href="http://bit.ly/ICIKapataganSHSEnrollmentForm" target="_blank">Kapatagan</a>
              </div>
            </div>
        </div>
  </div>
</section>

<section class="p-0 height-70 height-sm-50 height-xs-80">
    <div class="row align-items-center">
        <div class="branch-container align-items-center first">
           <div class="tab mt-4 mt-xs-0" style="z-index: 999;">
            <h2 class="mb-4">Visit ICI</h2>
              <button class="tablinks active" onmouseover="openCity(event, 'Iligan', 'ilg')">Iligan <i class="fas fa-arrow-right float-right mt-1 c-magenta someicon"></i></button> 
              <button class="tablinks" onmouseover="openCity(event, 'Kapatagan', 'kap')">Kapatagan <i class="fas fa-arrow-right float-right mt-1 c-magenta someicon"></i></button>
              <button class="tablinks" onmouseover="openCity(event, 'Cagayan', 'cdo')">Cagayan de Oro <i class="fas fa-arrow-right float-right mt-1 c-magenta someicon"></i></button>
            </div>



            <div id="Iligan" class="tabcontent" style="display: block;">
             
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3948.741599089479!2d124.23506211447973!3d8.22871959407845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x325575e72406c693%3A0x4716761795a73879!2sICI%20Main!5e0!3m2!1sen!2sph!4v1618385822843!5m2!1sen!2sph" width="100%" style="border:0; z-index: 999;" allowfullscreen="" loading="lazy" class="map-h"></iframe>
                <div class="branch-details" style="z-index: 999;">
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <h6><i class="fas fa-phone-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="">+63-917-322-4133</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex align-items-start">
                        <div>
                           <h6><i class="fas fa-map-marker-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="mb-0">ICI Main Office</h6>
                            <h6 class="font-regular mt-0">Ground floor MJK Building<br>Mabini corner M. H. Del Pilar<br>Iligan City 9200</h6>
                        </div>
                    </div>
                </div>
              
            </div>

            <div id="Kapatagan" class="tabcontent">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4347.410349100227!2d123.76911353148613!3d7.898540613503714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3254349409a5b261%3A0x6f80dee31c3a8dfa!2sKapatagan%20Town%20Center!5e0!3m2!1sen!2sph!4v1618388434516!5m2!1sen!2sph" width="100%" class="map-h" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              
                <div class="branch-details">
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <h6><i class="fas fa-phone-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="">+63-917-322-4135</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex align-items-start">
                        <div>
                           <h6><i class="fas fa-map-marker-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="mb-0">ICI Building</h6>
                            <h6 class="font-regular mt-0">Purok 13<br>Barangay Poblacion<br>Kapatagan 9214</h6>
                        </div>
                    </div>

                </div>
            </div>

            <div id="Cagayan" class="tabcontent">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d880.2024935642795!2d124.75442643953332!3d8.509617698156681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32ffec69adb933a3%3A0x81beeb8236c8694b!2sIligan%20Computer%20Institute%20(ICI)!5e0!3m2!1sen!2sph!4v1618388088328!5m2!1sen!2sph" width="100%" class="map-h" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              <div class="branch-details">
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <h6><i class="fas fa-phone-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="">+63-917-322-4134</h6>
                        </div>
                    </div>
                    
                    <div class="d-flex align-items-start">
                        <div>
                           <h6><i class="fas fa-map-marker-alt c-primary"></i></h6>
                        </div>
                        <div class="pl-3">
                            <h6 class="mb-0">ICI Building</h6>
                            <h6 class="font-regular mt-0">Zone 2<br>Barangay Bugo<br>Cagayan de Oro City 9000</h6>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        
          
          <div id="cdo" class="tabbg "> 
            <div class="background-image-holder visit-bg hidden-xs"> <img alt="background" src="img/ici-branch-cdo.jpg"> </div> 
            <div class="hidden-lg tab-mob"> <img alt="background" src="img/ici-branch-cdo.jpg"> </div> 
        </div>

          <div id="kap" class="tabbg"> 
            <div class="background-image-holder visit-bg hidden-xs"> <img alt="background" src="img/ici-branch-kap.jpg"> </div>
            <div class="hidden-lg tab-mob"> <img alt="background" src="img/ici-branch-kap.jpg">  </div>
          </div>

          <div id="ilg" class="tabbg" >
            <div class="background-image-holder visit-bg hidden-xs"><img alt="background" src="img/ici-branch-ilg.jpg"> </div>
            <div class="hidden-lg tab-mob"> <img alt="background" src="img/ici-branch-ilg.jpg"></div>
          </div>

    </div>
</section>

<section class="space--xs">
    <div class="container text-center justify-content-center">
        <h2 class="mb-5">#AsaNaTaBai</h2>

        <div class="hidden-lg hidden-md">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{asset('/img/gallery/insta-1.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-2.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-3.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-4.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-5.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-6.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-7.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
              <div class="carousel-item">
                <img src="{{asset('/img/gallery/insta-8.jpg')}}" class="d-block w-100" alt="#AsaNaTaBai">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>

        <div class="row hidden-xs hidden-sm">
          <div class="col-md-3 col-sm-6 mb-4">
              <a href="{{asset('/img/gallery/insta-1.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-1.jpg')}}"></a>
          </div>
           <div class="col-md-3 col-sm-6 mb-4">
              <a href="{{asset('/img/gallery/insta-2.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-2.jpg')}}"></a>
          </div>
           <div class="col-md-3 mb-4">
              <a href="{{asset('/img/gallery/insta-3.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-3.jpg')}}"></a>
          </div>
           <div class="col-md-3 mb-4">
              <a href="{{asset('/img/gallery/insta-4.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-4.jpg')}}"></a>
          </div>
          <div class="col-md-3">
              <a href="{{asset('/img/gallery/insta-5.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-5.jpg')}}"></a>
          </div>
           <div class="col-md-3">
              <a href="{{asset('/img/gallery/insta-6.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-6.jpg')}}"></a>
          </div>
           <div class="col-md-3">
              <a href="{{asset('/img/gallery/insta-7.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-7.jpg')}}"></a>
          </div>
           <div class="col-md-3">
              <a href="{{asset('/img/gallery/insta-8.jpg')}}" data-lightbox="gallery" data-title="#AsaNaTaBai"><img src="{{asset('/img/gallery/insta-8.jpg')}}"></a>
          </div>
        </div>
    </div>
</section>


@endsection

@push('after-scripts')
<script src="{{asset('js/flickity.min.js')}}"></script>
<script src="{{asset('js/granim.min.js')}}"></script>
<script>
     $('.owl-carousel').owlCarousel({
    loop:true,
    rewind: true,
    margin:10,
    responsiveClass:true,
    animateOut: 'fadeOut',
    autoplay:true,
    mouseDrag: false,
    dots: false,
    nav: false,
    autoplayTimeout:5000,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
</script>
<script>
    function openCity(evt, cityName, cityBG) {
  // Declare all variables
  var i, tabcontent, tabbg, tabmob, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  tabbg = document.getElementsByClassName("tabbg");
  tabmob = document.getElementsByClassName("tab-mob");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
    tabbg[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  document.getElementById(cityBG).style.display = "block";
  document.getElementsByClassName("tab-mob").style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<script>
    var granimInstance = new Granim({
        element: '#canvas-basic',
        direction: 'diagonal',
        isPausedWhenNotInView: true,
        states : {
            "default-state": {
                gradients: [
                    ['#ff9966', '#ff5e62'],
                    ['#00F260', '#0575E6'],
                    ['#e1eec3', '#f05053']
                ]
            }
        }
    });
    
</script>
@endpush