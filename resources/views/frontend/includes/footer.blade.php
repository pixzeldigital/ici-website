<footer class="bg-dark-3 space--xs">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-6">
				<a href="{{route('frontend.index')}}" class="navbar-brand"><img src="{{asset('/img/ICI-Logo-White.png')}}" class="logo" width="70%"></a><br>

				<small>© 2021 ICI. All Rights Reserved.</small>

				<div class="mt-2">
					<a href="https://www.facebook.com/myici" target="_blank" class="btn-social"><i class="fab fa-facebook-f"></i></a>
					<a href="https://www.youtube.com/user/icichannel" target="_blank" class="btn-social"><i class="fab fa-youtube"></i></a>
					<a href="https://www.instagram.com/my.ici/" target="_blank" class="btn-social"><i class="fab fa-instagram"></i></a>
				</div>
			</div>
			
			<div class="col-md-3 col-xs-6">
				<div>
					<h6>ILIGAN</h6>
				</div>
				<div class="d-flex align-items-start">
                        <div>
                           <p><i class="fas fa-map-marker-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold mb-0">ICI Main Office</p>
                            <p class="mt-0">Ground floor MJK Building<br>Mabini corner M. H. Del Pilar<br>Iligan City 9200</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div>
                            <p><i class="fas fa-mobile-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">+63-917-322-4133</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <p><i class="fas fa-phone-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">228-1008</p>
                        </div>
                    </div>
				
			</div>
			<div class="col-md-3 col-xs-6">
				<div>
					<h6>CDO</h6>
				</div>
				<div class="d-flex align-items-start">
                        <div>
                           <p><i class="fas fa-map-marker-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold mb-0">ICI Building</p>
                            <p class="mt-0">Zone 2<br>Barangay Bugo<br>Cagayan de Oro City 9000</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div>
                            <p><i class="fas fa-mobile-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">+63-917-322-4134</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <p><i class="fas fa-phone-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">882-0640</p>
                        </div>
                    </div>
			</div>
			<div class="col-md-3 col-xs-6">
				<div>
					<h6>KAPATAGAN</h6>
				</div>
				<div class="d-flex align-items-start">
                        <div>
                           <p><i class="fas fa-map-marker-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold mb-0">ICI Building</p>
                            <p class="mt-0">Purok 13<br>Barangay Poblacion<br>Kapatagan 9214</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div>
                            <p><i class="fas fa-mobile-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">+63-917-322-4135</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <p><i class="fas fa-phone-alt c-primary"></i></p>
                        </div>
                        <div class="pl-3">
                            <p class="font-extrabold ">227-9029</p>
                        </div>
                    </div>
			</div>
		</div>
		<hr>
		<div class="row pt-3">
			<div class="col-md-3 col-xs-6 mb-2">
				<p class="font-extrabold mb-0">About ICI</p>
				<ul class="footer-links">
					<li><a href="/about">At a Glance</a></li>
					<li><a href="/about#history">History</a></li>
					<li><a href="/about#mission">Mission</a></li>
					<li><a href="/about#locations">Locations</a></li>
					<li><a href="/about#leadership">Leadership</a></li>
					<li><a href="/about#faculty-staff">Faculty & Staff</a></li>
					<!-- <li><a href="">Community Involvement</a></li>
					<li><a href="">Gallery</a></li> -->
				</ul>
			</div>
			<div class="col-md-3 col-xs-6">
				<p class="font-extrabold mb-0">Admissions</p>
				<ul class="footer-links">
					<li><a href="/admissions">SHS Admission</a></li>
					<li><a href="/admissions/tesda">TESDA Admision</a></li>
					<li><a href="/admissions/scholarship">DepEd Scholarship</a></li>
					<li><a href="/admissions/crash-courses">Crash Courses</a></li>
					<li><a href="/admissions/faqs">Enrollment FAQs</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-xs-6">
				<p class="font-extrabold mb-0">Education</p>
				<ul class="footer-links">
					<li><a href="/education">Academics at ICI</a></li>
					<li><a href="/education/shs">Senior High School</a></li>
					<li><a href="/education/tesda">TESDA Programs</a></li>
					<li><a href="/education/short-courses">Crash Courses</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-xs-6">
				<p class="font-extrabold mb-0">Student Life</p>
				<ul class="footer-links">
					<li><a href="/student-life">Holistic Growth</a></li>
					<li><a href="/student-life#extracurriculars">Extracurricular for Students</a></li>
				</ul>

				<p class="font-extrabold mt-3 mb-0">Quick Links</p>
				<ul class="footer-links">
					<li><a href="/">Home</a></li>
					<li><a href="/gallery">Gallery</a></li>
					<li><a href="/contact-us">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>