<nav class="navbar navbar-expand-md navbar-light bg-main shadow-sm pt-0 pb-0 hidden-xs">
    <div class="container">
        <ul class="navbar-nav list-inline justify-content-lg-end  justify-content-start">

            <li class="nav-item">
                <span class="nav-link nav-secondary">ILIGAN</span>
            </li>
            <li class="nav-item">
                <span class="nav-link nav-secondary">KAPATAGAN</span>
            </li>
            <li class="nav-item">
                <span class="nav-link nav-secondary">CDO</span>
            </li>
        </ul>

        <ul class="navbar-nav list-inline justify-content-lg-end  justify-content-start hidden-xs">
            <li class="nav-item">
                <x-utils.link
                    :href="url('/')"
                    {{-- :active="activeClass(Route::is('frontend.index'))" --}}
                    :text="__('Home')"
                    class="nav-link nav-secondary" />
            </li>
            <li class="nav-item">
                <x-utils.link
                    :href="url('/gallery')"

                    :text="__('Gallery')"
                    class="nav-link nav-secondary" />
            </li>
            <!-- <li class="nav-item">
                <x-utils.link
                    :href="url('/')"

                    :text="__('Alumni')"
                    class="nav-link nav-secondary" />
            </li> -->
           <!--  <li class="nav-item">
                <x-utils.link
                    :href="url('/')"
                    {{-- :active="activeClass(Route::is('frontend.auth.login'))" --}}
                    :text="__('Social Responsibility')"
                    class="nav-link nav-secondary" />
            </li> -->
            <!-- <li class="nav-item">
                <x-utils.link
                    :href="url('/careers')"
                    {{-- :active="activeClass(Route::is('frontend.auth.login'))" --}}
                    :text="__('Careers')"
                    class="nav-link nav-secondary" />
            </li> -->
            <li class="nav-item">
                <x-utils.link
                    :href="url('/contact-us')"
                    :text="__('Contact Us')"
                    class="nav-link nav-secondary" 
                    style="padding-top: 11px;"/>
            </li>
            <!-- @guest
                    <li class="nav-item">
                        <x-utils.link
                            :href="route('frontend.auth.login')"
                            :active="activeClass(Route::is('frontend.auth.login'))"
                            :text="__('Login')"
                            class="nav-link nav-secondary" />
                    </li>

                    @if (config('boilerplate.access.user.registration'))
                        <li class="nav-item">
                            <x-utils.link
                                :href="route('frontend.auth.register')"
                                :active="activeClass(Route::is('frontend.auth.register'))"
                                :text="__('Register')"
                                class="nav-link nav-secondary" />

                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <x-utils.link
                            href="#"
                            id="navbarDropdown"
                            class="nav-link dropdown-toggle"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            v-pre
                        >
                            <x-slot name="text">
                                <img class="rounded-circle" style="max-height: 20px" src="{{ $logged_in_user->avatar }}" />
                                {{ $logged_in_user->name }} <span class="caret"></span>
                            </x-slot>
                        </x-utils.link>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if ($logged_in_user->isAdmin())
                                <x-utils.link
                                    :href="route('admin.dashboard')"
                                    :text="__('Administration')"
                                    class="dropdown-item" />
                            @endif

                            @if ($logged_in_user->isUser())
                                <x-utils.link
                                    :href="route('frontend.user.dashboard')"
                                    :active="activeClass(Route::is('frontend.user.dashboard'))"
                                    :text="__('Dashboard')"
                                    class="dropdown-item"/>
                            @endif

                            <x-utils.link
                                :href="route('frontend.user.account')"
                                :active="activeClass(Route::is('frontend.user.account'))"
                                :text="__('My Account')"
                                class="dropdown-item" />

                            <x-utils.link
                                :text="__('Logout')"
                                class="dropdown-item"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <x-slot name="text">
                                    @lang('Logout')
                                    <x-forms.post :action="route('frontend.auth.logout')" id="logout-form" class="d-none" />
                                </x-slot>
                            </x-utils.link>
                        </div>
                    </li>
                @endguest -->

            <li class="nav-item">
                <a class="nav-link nav-social" href="https://www.facebook.com/myici" target="_blank"><i class="fab fa-facebook"></i></a>
            </li>
             <li class="nav-item">
                <a class="nav-link nav-social" href="https://www.instagram.com/my.ici/" target="_blank"><i class="fab fa-instagram"></i></a>
            </li>
             <li class="nav-item">
                <a class="nav-link nav-social" href="https://www.youtube.com/user/icichannel" target="_blank"><i class="fab fa-youtube"></i></a>
            </li>
        </ul>

    </div>
</nav>

<div class="hidden-lg hidden-sm hidden-md bg-main">
    <div class="row justify-content-between">
        <div class="col-6 navbar nav-m">
            <a class="nav-mobile nav-secondary">ILIGAN</a>
            <a class="nav-mobile nav-secondary">Kapatagan</a>
            <a class="nav-mobile nav-secondary">CDO</a>
        </div>
        <div class="col-3 navbar nav-m">
                <a class="nav-mobile nav-social" href="https://www.facebook.com/myici" target="_blank"><i class="fab fa-facebook"></i></a>
                <a class="nav-mobile nav-social" href="#"><i class="fab fa-instagram"></i></a>
                <a class="nav-mobile nav-social" href="https://www.youtube.com/user/icichannel" target="_blank"><i class="fab fa-youtube"></i></a>
        </div>
    </div>

</div>

<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
    <div class="container">
        <!-- <x-utils.link
            :href="route('frontend.index')"
            :text="appName()"
            class="navbar-brand" /> -->

        <a href="{{route('frontend.index')}}" class="navbar-brand"><img src="{{asset('img/ICI-Logo-Final.png')}}" class="logo"></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="@lang('Toggle navigation')">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              
                <li class="nav-item">
                    <x-utils.link
                        :href="url('/about')"
                        :active="activeClass(Route::is('frontend.pages.about'))"
                        :text="__('About ICI')"
                        class="nav-link nav-main" />

                </li>

                <li class="nav-item">
                    <x-utils.link
                        :href="url('admissions')"
                        :active="activeClass(Route::is('frontend.pages.admissions.index'))"
                        :text="__('Admissions')"
                        class="nav-link nav-main" />

                </li>

                <li class="nav-item">
                    <x-utils.link
                        :href="url('/education')"
                        :active="activeClass(Route::is('frontend.pages.education.index'))"
                        :text="__('Education')"
                        class="nav-link nav-main" />

                </li>

                <li class="nav-item">
                    <x-utils.link
                        :href="url('student-life')"
                        :active="activeClass(Route::is('frontend.pages.student-life.index'))"
                        :text="__('Student Life')"
                        class="nav-link nav-main" />

                </li>

                <!-- <li class="nav-item">
                    <x-utils.link
                        :href="url('news')"
                        :active="activeClass(Route::is('frontend.auth.register'))"
                        :text="__('News')"
                        class="nav-link nav-main" />

                </li> -->



                <div class="hidden-lg hidden-md hidden-sm">
                    <li class="nav-item">
                    <x-utils.link
                        :href="url('/gallery')"
                        {{-- :active="activeClass(Route::is('frontend.auth.login'))" --}}
                        :text="__('Gallery')"
                        class="nav-link nav-main" />
                </li>

                    <li class="nav-item">
                        <x-utils.link
                            :href="url('/contact-us')"
                            {{-- :active="activeClass(Route::is('frontend.auth.login'))" --}}
                            :text="__('Contact Us')"
                            class="nav-link nav-main" />
                    </li>
                   <!--  @guest
                            <li class="nav-item">
                                <x-utils.link
                                    :href="route('frontend.auth.login')"
                                    :active="activeClass(Route::is('frontend.auth.login'))"
                                    :text="__('Login')"
                                    class="nav-link nav-main" />
                            </li>

                            @if (config('boilerplate.access.user.registration'))
                                <li class="nav-item">
                                    <x-utils.link
                                        :href="route('frontend.auth.register')"
                                        :active="activeClass(Route::is('frontend.auth.register'))"
                                        :text="__('Register')"
                                        class="nav-link nav-main" />

                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <x-utils.link
                                    href="#"
                                    id="navbarDropdown"
                                    class="nav-link dropdown-toggle"
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    v-pre
                                >
                                    <x-slot name="text">
                                        <img class="rounded-circle" style="max-height: 20px" src="{{ $logged_in_user->avatar }}" />
                                        {{ $logged_in_user->name }} <span class="caret"></span>
                                    </x-slot>
                                </x-utils.link>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if ($logged_in_user->isAdmin())
                                        <x-utils.link
                                            :href="route('admin.dashboard')"
                                            :text="__('Administration')"
                                            class="dropdown-item" />
                                    @endif

                                    @if ($logged_in_user->isUser())
                                        <x-utils.link
                                            :href="route('frontend.user.dashboard')"
                                            :active="activeClass(Route::is('frontend.user.dashboard'))"
                                            :text="__('Dashboard')"
                                            class="dropdown-item"/>
                                    @endif

                                    <x-utils.link
                                        :href="route('frontend.user.account')"
                                        :active="activeClass(Route::is('frontend.user.account'))"
                                        :text="__('My Account')"
                                        class="dropdown-item" />

                                    <x-utils.link
                                        :text="__('Logout')"
                                        class="dropdown-item"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <x-slot name="text">
                                            @lang('Logout')
                                            <x-forms.post :action="route('frontend.auth.logout')" id="logout-form" class="d-none" />
                                        </x-slot>
                                    </x-utils.link>
                                </div>
                            </li>
                        @endguest -->
                        </div>

                        <li class="nav-item">
                           <!--  <x-utils.link
                                :href="url('/admissions')"
                                :active="activeClass(Route::is('frontend.auth.register'))"
                                :text="__('Enroll Now')"
                                role="button"
                                class="nav-link nav-main btn btn-nav btn-primary" /> -->

                                <div class="dropdown">
  <a class="nav-link nav-main btn btn-nav btn-primary dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Register Now &nbsp; <i class="fas fa-chevron-down pt-1"></i>
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="http://bit.ly/ICIILIGANOnlineReg2021" target="_blank">Iligan</a>
    <a class="dropdown-item" href="http://bit.ly/ICICDOOnlineReg2021" target="_blank">Cagayan de Oro</a>
    <a class="dropdown-item" href="http://bit.ly/ICIKapataganSHSEnrollmentForm" target="_blank">Kapatagan</a>
  </div>
</div>

                        </li>

                
            </ul>
        </div><!--navbar-collapse-->
    </div><!--container-->
</nav>
