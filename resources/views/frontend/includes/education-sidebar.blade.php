<div class="col-md-2  top p-0">
	<div class="boxed pos--sticky">
		<a href="/education" class="c-gray">Academics at ICI</a>
		<hr>
		<a href="/education/shs" class="c-gray" active="{{activeClass(Route::is('frontend.pages.education.shs'))}}">Senior High School Programs</a>
		<hr>
		<a href="/education/tesda" class="c-gray">TESDA Programs</a>
		<hr>
		<a href="/education/short-courses" class="c-gray">Crash Courses</a>
	</div>
</div>