@extends('frontend.layouts.app')

@section('title', __('Careers'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/careers-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Careers</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2 class="mb-3">Teach at ICI</h2>

				<p class="mb-3">We are always on the lookout for talented instructors to join ICI. Interested applicants may send their cover letter, resume, scanned TOR and college diploma, as well as other certifications to <a href="mailto:hr@ici.edu.ph">hr@ici.edu.ph</a>.</p>

				<p class="mb-1">We also accept walk-in applicants in our offices. Please bring the following:</p>
				<ul>
					<li>Resume</li>
					<li>TOR</li>
					<li>College Diploma</li>
					<li>Optional/Good to have: Certifications</li>
				</ul>

				<h2 class="mt-5">Current Openings</h2>
				<p class="mb-3">We have no current openings in any of our branches.</p>

			</div>
			<div class="col-md-4">
				<img src="{{asset('img/about/faculty.jpg')}}">
				<img src="{{asset('img/about/leadership.jpg')}}">
			</div>
		</div>
	</div>
</section>

<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/career-banner.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-10">
        <h4 class="mb-4">Follow us on social media to know when we have new openings</h4>

        <div class="row">
        	<div class="col-md-4 pl-0">
        		<h5 class="mb-4"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/myici" class="c-bod" style="color: white !important;">Iligan Branch</a></h5>
        	</div>

        	<div class="col-md-4 pl-0">
        		<h5 class="mb-4"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/myicikap/" class="c-bod" style="color: white !important;">Kapatagan Branch</a></h5>
        	</div>

        	<div class="col-md-4 pl-0">
        		<h5 class="mb-4"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/IliganComputerInstituteCDO" class="c-bod" style="color: white !important;">CDO Branch</a></h5>
        	</div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

