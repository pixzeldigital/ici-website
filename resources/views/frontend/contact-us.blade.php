@extends('frontend.layouts.app')

@section('title', __('Contact Us'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/contact-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Contact Us</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h4 class=" mb-4">Iligan</h4>

				<div class="row">
					<div class="col-md-6 pl-0">

					<h6 class="c-primary mb-0">General Information </h6>
					<p class="mb-4">0917-322-4133</p>

					<h6 class="c-primary mb-0">Business Center</h6>
					<p class="mb-4">228-1008</p>

					<h6 class="c-primary mb-0">VP for Operations</h6>
					<p class="mb-4">0917-621-1599</p>

					<h6 class="c-primary mb-0">Principal’s Office</h6>
					<p class="mb-0">0917-707-2004</p>
					<p class="mb-4">shs.principal@ici.edu.ph</p>

					<h6 class="c-primary mb-0">Registrar’s Office</h6>
					<p class="mb-4">registrar@ici.edu.ph</p>

					<h6 class="c-primary mb-0">Guidance Office</h6>
					<p class="mb-0">0917-712-8298</p>
					<p class="mb-0">0917-712-7524</p>
					<p class="mb-4">guidance.ilg@ici.edu.ph</p>
				</div>
				<div class="col-md-6 pl-0">


						<h6 class="c-primary mb-0">Main</h6>
						<p class="mb-0">MJK Building</p>
						<p class="mb-0">Mabini corner M. H. Del Pilar St</p>
						<p class="mb-4">228-1299</p>

						<h6 class="c-primary mb-0">Annex 1 </h6>
						<p class="mb-0">CCCI Building</p>
						<p class="mb-0">Benito Labao St.</p>
						<p class="mb-4">222-2908</p>

						<h6 class="c-primary mb-0">Annex 2</h6>
						<p class="mb-0">MCK Building</p>
						<p class="mb-4">Mariano Badelles St.</p>

						<h6 class="c-primary mb-0">Official Facebook Page</h6>
						<p class="mb-4"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/myici" class="c-bod">@myici</a></p>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<h4 class=" mb-4">CDO</h4>

				<h6 class="c-primary mb-0">General Information</h6>
				<p class="mb-4">(063) 882-0640</p>

				<h6 class="c-primary mb-0">Branch Manager/Cashier’s Office </h6>
				<p class="mb-4">0917-322-4134</p>

				<h6 class="c-primary mb-0">SHS Coordinator</h6>
				<p class="mb-4">0917-154-4752</p>

				<h6 class="c-primary mb-0">Guidance Office</h6>
				<p class="mb-0">0917-154-4399</p>
				<p class="mb-4">guidance.cdo@ici.edu.ph</p>

				<h6 class="c-primary mb-0">Tech Support</h6>
				<p class="mb-4">0917-154-4075</p>

				<h6 class="c-primary mb-0">Official Facebook Page</h6>
				<p class="mb-5"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/myicicdo" class="c-bod">@myicicdo</a></p>

				
			</div>

			<div class="col-md-3">
				<h4 class=" mb-4">Kapatagan</h4>

				<h6 class="c-primary mb-0">General Information</h6>
				<p class="mb-4">227-9029</p>

				<h6 class="c-primary mb-0">Branch Manager</h6>
				<p class="mb-4">0917-322-4135</p>

				<h6 class="c-primary mb-0">Help Desk</h6>
				<p class="mb-4">0917-120-0904</p>

				<h6 class="c-primary mb-0">Official Facebook Page</h6>
				<p class="mb-4"><i class="fab fa-facebook c-fb"></i> <a href="https://www.facebook.com/myicikap/" class="c-bod">@myicikap</a></p>
			</div>
			
		</div>

		<div class="row align-content-center pt-4">
		<div class="col-lg-6 col-md-8">
			<p class="mb-3">Any questions, suggestions, complaints and recommendations? Fill up this form and we will get back to you as soon as possible.</p>

			<form>
			  <div class="form-row">
			    <div class="form-group col-md-12">
			      <input type="text" class="form-control" id="inputEmail4" placeholder="Name">
			    </div>
			    <div class="form-group col-md-12">
			      <input type="email" class="form-control" id="inputPassword4" placeholder="Email">
			    </div>
			    <div class="form-group col-md-12">
			      <input type="text" class="form-control" id="inputPassword4" placeholder="Contact Number">
			    </div>
			    <div class="form-group col-md-12">
			      <textarea class="form-control" rows="4">Message</textarea>
			    </div>
			    <button class="btn btn-primary btn-block" type="submit">Submit</button>
			  </div>
			</form>

		</div>

		<div class="col-lg-6 col-md-4 mt-4">
			<img src="{{asset('img/gallery/14.jpg')}}">
		</div>
	</div>
	</div>
</section>

@endsection

