@extends('frontend.layouts.app')

@section('title', __('Gallery'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/gallery/hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Gallery</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-12">

				<h3 class="mb-3">Facilities</h3>

				<ul class="nav nav-pills mb-4 flex-column flex-sm-row" id="pills-tab" role="tablist">
					
					  <li class="nav-item flex-sm-fill text-center">
					    <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#cdo" role="tab" aria-controls="pills-profile" aria-selected="true">Cagayan de Oro</a>
					  </li>
					  <li class="nav-item flex-sm-fill text-center">
					    <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#iligan" role="tab" aria-controls="pills-home" aria-selected="true">Iligan</a>
					  </li>
					  <li class="nav-item flex-sm-fill text-center">
					    <a class="nav-link " id="pills-profile-tab" data-toggle="pill" href="#kapatagan" role="tab" aria-controls="pills-profile" aria-selected="false">Kapatagan</a>
					  </li>
				</ul>

				<div class="tab-content" id="pills-tabContent">
  					<div class="tab-pane fade show active" id="cdo" role="tabpanel" aria-labelledby="pills-home-tab">

  						<div class="row">
  							<div class="col-md-6 mb-4">

  						<h4>Nenita's Kitchen (HRM Laboratory)</h4>

  						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/cdo-1.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-2.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-6.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-7.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-15.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
					<div class="col-md-6 mb-4">

						<h4>Kitchen Pantry</h4>

  						<div id="pantry" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/cdo-8.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-9.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-10.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-11.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-12.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#pantry" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#pantry" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>

					<div class="row">
  							<div class="col-md-6 mb-4">

  						<h4>Laboratory Rooms</h4>

  						<div id="cdo-rooms" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/cdo-22.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-24.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-26.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-28.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-30.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-31.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-29.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#cdo-rooms" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#cdo-rooms" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
					<div class="col-md-6 mb-4">

						<h4>Library</h4>

  						<div id="library" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						  	<div class="carousel-item active">
						      <img src="{{asset('/img/gallery/cdo-34.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-33.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-35.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/cdo-36.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#library" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#library" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>
			</div>

  					<div class="tab-pane fade" id="iligan" role="tabpanel" aria-labelledby="pills-home-tab">

  						<div class="row">
	  						<div class="col-md-6 mb-4">

	  						<h4>Nenita's Kitchen (HRM Laboratory)</h4>

	  						<div id="ilg-nena" class="carousel slide" data-ride="carousel">
	  
							  <div class="carousel-inner">
							    <div class="carousel-item active">
							      <img src="{{asset('/img/gallery/ilg-15.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-16.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-17.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							     <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-18.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-19.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							  </div>
							  <a class="carousel-control-prev" href="#ilg-nena" role="button" data-slide="prev">
							    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#ilg-nena" role="button" data-slide="next">
							    <span class="carousel-control-next-icon" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
					<div class="col-md-6 mb-4">

						<h4>Kitchen Pantry</h4>

  						<div id="ilg-pantry" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/ilg-6.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-7.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-8.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-9.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-10.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-11.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-12.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-13.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#ilg-pantry" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#ilg-pantry" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>

  					<div class="col-md-6 mb-4">

  						<h4>Lecture Rooms</h4>

  						<div id="ilg-rooms" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/ilg-1.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-2.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-3.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-4.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-5.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-14.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#ilg-rooms" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#ilg-rooms" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
					<div class="col-md-6 mb-4">

	  						<h4>Computer Laboratory</h4>

	  						<div id="ilg-comp" class="carousel slide" data-ride="carousel">
	  
							  <div class="carousel-inner">
							    <div class="carousel-item active">
							      <img src="{{asset('/img/gallery/ilg-20.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-21.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-22.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							     <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-23.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-24.jpg')}}" class="d-block w-100" alt="...">
							    </div>
							    <div class="carousel-item">
							      <img src="{{asset('/img/gallery/ilg-25.jpg')}}" class="d-block w-100" alt="...">
							    </div>

							  </div>
							  <a class="carousel-control-prev" href="#ilg-comp" role="button" data-slide="prev">
							    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#ilg-comp" role="button" data-slide="next">
							    <span class="carousel-control-next-icon" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
					<div class="row">
	  					
					<div class="col-md-6 mb-4">

						<h4>Library</h4>

  						<div id="ilg-library" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						  	<div class="carousel-item active">
						      <img src="{{asset('/img/gallery/ilg-26.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-27.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-28.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#ilg-library" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#ilg-library" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>

					<div class="col-md-6 mb-4">

						<h4>Practical Nursing Laboratory</h4>

  						<div id="ilg-nurlab" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						  	<div class="carousel-item active">
						      <img src="{{asset('/img/gallery/ilg-29.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-30.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/ilg-31.jpg')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#ilg-nurlab" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#ilg-nurlab" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>
			</div>
		</div>


  					<div class="tab-pane fade" id="kapatagan" role="tabpanel" aria-labelledby="pills-home-tab">

  						<div class="row">
  							<div class="col-md-6 mb-4">

  						<h4>Nenita's Kitchen (HRM Laboratory)</h4>

  						<div id="kap-kitchen" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/kap-20.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-5.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-6.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-7.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-8.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-9.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-10.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-11.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-14.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-15.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#kap-kitchen" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#kap-kitchen" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
					<div class="col-md-6 mb-4">

						<h4>Lecture Rooms</h4>

  						<div id="kaplecture" class="carousel slide" data-ride="carousel">
  
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img src="{{asset('/img/gallery/kap-1.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-2.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-3.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						     <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-4.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-17.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						    <div class="carousel-item">
						      <img src="{{asset('/img/gallery/kap-18.JPG')}}" class="d-block w-100" alt="...">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#kaplecture" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#kaplecture" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
  					</div>
  				</div>

  				<div class="tab-content" id="pills-tabContent">
  					
  				</div>

			</div>
		</div>
	</div>
</section>
@endsection

@push('after-scripts')
<script>
	showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

@endpush