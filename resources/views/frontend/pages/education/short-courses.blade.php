@extends('frontend.layouts.app')

@section('title', __('Crash Courses'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/education/education-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Education</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-10">
				<h1 class="mb-4">Crash Courses</h1>

					<h4 class="mb-3 mt-4 c-primary">Available Subjects in Iligan</h4>

					<table class="table table-sm">
					  <thead class="bg-secondary-2">
					    <tr>
					      <th scope="col" class="c-white font-bold">Duration</th>
					      <th scope="col" class="c-white font-bold">Pricing per subject</th>
					      <th scope="col" class="c-white font-bold">Subjects</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr class="bg-light">
					      <th scope="row" class="font-bold">10 hours</th>
					      <td>P 2,000</td>
					      <td>
					      	<p class="mb-0">MS Windows</p>
					      	<p class="mb-0">MS Words</p>
					      	<p class="mb-0">MS Powerpoint</p>
					      	<p class="mb-0">MS Excel</p>
					      </td>
					    </tr>
					    <tr>
					      <th scope="row" rowspan="2" class="font-bold">20 hours</th>
					      <td>P 4,500</td>
					      <td>
					      	<div class="row">
					      		<div class="col-md-6 pl-0">
					      			<p class="mb-0">HTML & CSS</p>
							      	<p class="mb-0">WordPress</p>
							      	<p class="mb-0">Basic PHP</p>
							      	<p class="mb-0">Advanced PHP (Database)</p>
					      		</div>
					      		<div class="col-md-6 pl-0">
					      			<p class="mb-0">SEO & Internet Marketing</p>
							      	<p class="mb-0">PC Repair</p>
							      	<p class="mb-0">Adobe Photoshop</p>
							      </div>
					      	</div>
					      	
					      	
					      </td>
					    </tr>
					    <tr>
					      <td>P 5,900</td>
					      <td>
					      	<p class="mb-0">Basic AutoCAD</p>
					      	<p class="mb-0">Advanced AutoCAD (3D Modeling)</p>
					      </td>
					    </tr>

					    <tr class="bg-light">
					      <th scope="row" rowspan="2" class="font-bold">40 hours</th>
					      <td>P 9,500</td>
					      <td>
					      	<p class="mb-0">Housekeeping</p>
					      </td>
					    </tr>
					    <tr class="bg-light">
					      <td>P 14,500</td>
					      <td>
					      	<p class="mb-0">Culinary Arts: Cookery<span class="c-magenta">*</span></p>
					      	<p class="mb-0">Bread & Pastry Production<span class="c-magenta">*</span></p>
					      	<p class="mb-0 small"><span class="c-magenta">*</span>Ingredients are not included in pricing. </p>
					      </td>
					    </tr>
					    <tr>
					      <th scope="row" class="font-bold">50 hours</th>
					      <td>P 11,600</td>
					      <td>
					      	<p class="mb-0">Bookkeeping</p>
					      	<p class="mb-0">Contact Center</p>
					      	<p class="mb-0">Food & Beverage Services</p>
					      	<p class="mb-0">Bartending</p>
					      </td>
					    </tr>
					    <tr class="bg-light">
					      <th scope="row" class="font-bold">3 months</th>
					      <td>P 14,590</td>
					      <td>
					      	<p class="mb-0">Culinary Arts<span class="c-magenta">**</span></p>
					      	<p class="mb-0 small"><span class="c-magenta">**</span>Ingredients are not included in pricing. </p>
					      </td>
					    </tr>
					  </tbody>
					</table>

				<h4 class="mb-3 mt-4 c-primary">Available Subjects in CDO</h4>

					<table class="table table-sm">
					  <thead class="bg-secondary-2">
					    <tr>
					      <th scope="col" width="12%" class="c-white font-bold">Duration</th>
					      <th scope="col" width="22%" class="c-white font-bold">Pricing per subject</th>
					      <th scope="col" class="c-white font-bold">Subjects</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr class="bg-light">
					      <th scope="row" class="font-bold">10 hours</th>
					      <td>P 2,000</td>
					      <td>
					      	<p class="mb-0">MS Windows</p>
					      	<p class="mb-0">MS Words</p>
					      	<p class="mb-0">MS Powerpoint</p>
					      	<p class="mb-0">MS Excel</p>
					      </td>
					    </tr>
					    <tr>
					      <th scope="row" rowspan="2" class="font-bold">20 hours</th>
					      <td>P 4,000</td>
					      <td>
					      	<p class="mb-0">PC Repair & Troubleshooting</p>
							 <p class="mb-0">Networking</p>
					      </td>
					    </tr>
					    <tr>
					      <td>P 4,500</td>
					      <td>
					      	<p class="mb-0">AutoCAD</p>
					      </td>
					    </tr>
					</tbody>
				</table>

				<h4 class="mb-3 mt-4 c-primary">Available Subjects in Kapatagan</h4>

					<table class="table table-sm">
					  <thead class="bg-secondary-2">
					    <tr>
					      <th scope="col" width="12%" class="c-white font-bold">Duration</th>
					      <th scope="col" width="22%" class="c-white font-bold">Pricing per subject</th>
					      <th scope="col" class="c-white font-bold">Subjects</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr class="bg-light">
					      <th scope="row" class="font-bold">20 hours</th>
					      <td>P 4,500</td>
					      <td>
					      	<div class="row">
					      		<div class="col-md-6 pl-0">
					      			<p class="mb-0">Basic AutoCAD</p>
									 <p class="mb-0">Advanced AutoCAD</p>
							      	<p class="mb-0">Video Editing</p>
							      	<p class="mb-0">Adobe Illustrator</p>
					      		</div>
					      		<div class="col-md-6 pl-0">
					      			<p class="mb-0">Adobe Photoshop</p>
							      	<p class="mb-0">PC Troubleshooting</p>
							      	<p class="mb-0">Computer System Servicing</p>
							      	<p class="mb-0">Visual Graphic Design</p>
							      </div>
					      	</div>
					      	
					      </td>
					    </tr>
					    <tr>
					      <th scope="row" class="font-bold">30 hours</th>
					      <td>P 6,000</td>
					      <td>
					      	<p class="mb-0">MS Words</p>
					      	<p class="mb-0">MS Powerpoint</p>
					      	<p class="mb-0">MS Excel</p>
					      </td>
					    </tr>
					    <tr class="bg-light">
					    	<th scope="row" class="font-bold">80 hours</th>
					      <td>P 18,000</td>
					      <td>
					      	<p class="mb-0">Computer System Servicing</p>
					      	<p class="mb-0">Visual Graphic Design</p>
					      </td>
					    </tr>
					</tbody>
				</table>
			</div>
			@include('frontend.includes.education-sidebar')
		</div>
	</div>
</section>
@endsection