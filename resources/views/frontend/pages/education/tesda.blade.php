@extends('frontend.layouts.app')

@section('title', __('TESDA Programs'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/education/education-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Education</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-10">
				<h1>TESDA Programs</h1>

					<div class="card p-3 mt-4 bg-secondary">
						<h5 class="">Notice: Enrollment for our TESDA programs this upcoming school year</h5>

						<p class="mt-3 mb-0">For SY 2021-2022, we will not be accepting walk-in applications for our TESDA courses. We will only accept applicants with valid TESDA scholarships. To apply, email <span class="c-primary">registrar@ici.edu.ph</span> with your documents.</p>
					</div>

					

					<p class="mt-3">We offer the most in-demand TESDA courses for individuals who want to jumpstart their career. </p>

					

					<h4 class="mb-4 mt-4 c-blue">Courses in Iligan</h4>

					<div class="row">
						<div class="col-md-6 pl-0 first">
							<img src="{{asset('img/education/hr.jpg')}}" width="90%">
							<p class="mt-2 mb-0"><span class="font-bold">Industry</span> Tourism</p>
					        <p class="mb-0"><span class="font-bold">Course Length</span> 2 years</p>

							<h3 class="pt-3 font-regular c-primary">Hotel and Restaurant Services</h3>
							
					        <p class="mb-3"><span class="font-bold">Qualifications</span> BREAD AND PASTRY PRODUCTION NC II, COOKERY NC II, FOOD AND BEVERAGE SERVICES NC II, BARTENDING NC II</p>
					        <p class="mb-0">Accelerate your career in the tourism industry through this 2-year course. Gain the skills and competencies to excel even in the changing global environment. </p>
						</div>
						<div class="col-md-6 pl-0 second">
							<img src="{{asset('img/education/caregiver.jpg')}}" width="90%">
							<p class="mt-2 mb-0"><span class="font-bold">Industry</span> Healthcare</p>
					        <p class="mb-0"><span class="font-bold">Course Length</span> 6 months</p>

							<h3 class="pt-3 font-regular c-primary">Caregiving</h3>
							
					        <p class="mb-3"><span class="font-bold">Qualifications</span> CAREGIVING NC II</p>
					        <p class="mb-0">Are you planning to work overseas as a healthcare professional? This course is perfect for those who want to grab that opportunity and become a well-trained caregiver.</p>
						</div>
						<div class="col-md-6 mt-5 pl-0 first">
							<img src="{{asset('img/education/computer.jpg')}}" width="90%">
							<p class="mt-2 mb-0"><span class="font-bold">Industry</span> Electrical and Electronics</p>
					        <p class="mb-0"><span class="font-bold">Course Length</span> 1 year</p>

							<h3 class="pt-3 font-regular c-primary">Computer Systems Servicing</h3>
							
					        <p class="mb-3"><span class="font-bold">Qualifications</span> COMPUTER SYSTEMS SERVICING NC II</p>
					        <p class="mb-0">Master how to install and configure computer systems, set up networks and servers, as well as learn how to maintain and repair them in this course. </p>
						</div>
						<div class="col-md-6 mt-5 pl-0 second">
							<img src="{{asset('img/education/graphic.jpg')}}" width="90%">
							<p class="mt-2 mb-0"><span class="font-bold">Industry</span> Information and Communication Technologies</p>
					        <p class="mb-0"><span class="font-bold">Course Length</span> 1 year</p>

							<h3 class="pt-3 font-regular c-primary">Visual Graphic Design</h3>
							
					        <p class="mb-3"><span class="font-bold">Qualifications</span> VISUAL GRAPHIC DESIGN NC III</p>
					        <p class="mb-0">Equip yourself with new knowledge on graphic design for electronic and print media, packages, and displays. In this course, you will learn about various design techniques and design strategy. </p>
						</div>
					</div>

					<!-- <h4 class="mb-1 mt-4 c-primary">TESDA Courses in CDO</h4>

					<div class="row">
						<div class="col-md-6 pl-0">
							<h6 class="pt-3">2 Year Courses</h6>
							 <p class="mb-0">Information Technology</p>
					        <p class="mb-0">Computer Engineering Technology</p>
					        <p class="mb-0">Digital Media Arts</p>
					       
						</div>
						<div class="col-md-6 pl-0">
							<h6 class="pt-3">1 Year Courses</h6>
							<p class="mb-0">Computer Programming</p>
							<p class="mb-0">Visual Graphic Design NC III</p>
					        <p class="mb-0">Computer Systems Servicing</p>
					        <p class="mb-0">2D Animation</p>
						</div>
					</div>

					<h4 class="mb-1 mt-4 c-primary">TESDA Courses in Kapatagan</h4>

					<div class="row">
						<div class="col-md-6 pl-0">
							<h6 class="pt-3">2 Year Courses</h6>
							 <p class="mb-0">Information Technology</p>
					        <p class="mb-0">Computer Engineering</p>
					        <p class="mb-0">Digital Media Arts</p>
					        <p class="mb-0">Hotel and Restaurant Management</p>
					        <p class="mb-0">Office Management</p>
					       
						</div>
						<div class="col-md-6 pl-0">
							<h6 class="pt-3">1 Year Courses</h6>
							<p class="mb-0">Computer Programming</p>
							<p class="mb-0">Visual Graphic Design</p>
					        <p class="mb-0">Computer Network Services</p>
					        <p class="mb-0">Computer Secretarial</p>
						</div>
					</div> -->
			</div>
			@include('frontend.includes.education-sidebar')
		</div>
	</div>
</section>
@endsection