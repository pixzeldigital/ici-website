@extends('frontend.layouts.app')

@section('title', __('Education'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/education/education-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Education</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10">
				<h1>Explore our Programs</h1>

				<div class="row pb-5 d-flex">
					<div class="col-md-4 pt-3 pl-0 first">
						<a href="/education/shs"><img src="{{asset('/img/education/shs.jpg')}}"></a>
						<a href="/education/shs" class="btn btn-primary btn-square btn-block">Senior High School</a>
					</div>

					<div class="col-md-4 pt-3 pl-0 second">
						<a href="/education/tesda"><img src="{{asset('/img/education/tesda.jpg')}}"></a>
						<a href="/education/tesda" class="btn btn-blue btn-square btn-block">TESDA Programs</a>
					</div>
					<div class="col-md-4 pt-3 pl-0 third">
						<a href="/education/short-courses"><img src="{{asset('/img/education/crash.jpg')}}"></a>
						<a href="/education/short-courses" class="btn btn-mag btn-square btn-block">Crash Courses</a>
					</div>
				</div>



					<h1>Academics at ICI</h1>

					<h4 class="mb-3 mt-4 c-primary">Approach</h4>

					<div class="row pt-3">
						<div class="col-md-6 pl-0 first">
							<img src="{{asset('/img/education/hands-on.jpg')}}" class="">
						</div>
						<div class="col-md-6  second">
							<h5 class=" pt-5 pt-xs-0">Hands on Learning</h5>
							<p>We believe in learning by doing – especially when students’ interest in their lessons are stimulated. Through our hands-on approach, we immerse our students in the different subjects that they take and nurture well-rounded characters.</p>
						</div>
					</div>

					<div class="row pt-3">
						<div class="col-md-6 pl-0 first">
							<h5 class="pt-xs-0 pt-4">Industry Standard Facilities</h5>
							<p>Our students get to apply their classroom knowledge through practical tests using the school’s industry standard facilities. We consistently make an effort to upgrade our equipment and amenities for our students. </p>

							<a href="/gallery" class="btn btn-primary">View Gallery</a>
						</div>
						<div class="col-md-6 pl-0 mt-4 second">
							<img src="{{asset('/img/education/facility.jpg')}}" class="">
						</div>
					</div>

					<div class="row pt-4">
						<div class="col-md-6 pl-0 first">
							<img src="{{asset('/img/education/real-life.jpg')}}" class="">
						</div>
						<div class="col-md-6  second">
							<h5 class="pt-xs-0 pt-5">Real Life Application</h5>
							<p>Beyond the classroom, we want our students to excel in their pursuits and become valuable assets in their professional lives. We believe in setting up a solid foundation for our students’ future and we do this by putting their skills into practice.</p>
						</div>
					</div>

					<h4 class="mb-3 mt-5 c-primary">ICI graduates are well-rounded, well-trained, and self-directed individuals. </h4>

					<div class="row pt-4">
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-4.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-4.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-10.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-10.jpg')}}" ></a>
					</div>

					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-5.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-5.jpg')}}" ></a>
					</div>
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-2.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-2.jpg')}}" ></a>
					</div>
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-6.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-6.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-11.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-11.jpg')}}" ></a>
					</div>
					
					
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/11.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/11.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-8.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-8.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/education/educ-7.jpg')}}" data-lightbox="ilg" data-title="Academics at ICI"><img src="{{asset('/img/education/educ-7.jpg')}}" ></a>
					</div>
				</div>
			</div>
			@include('frontend.includes.education-sidebar')
		</div>
	</div>
</section>

@endsection