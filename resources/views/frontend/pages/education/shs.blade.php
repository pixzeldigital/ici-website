@extends('frontend.layouts.app')

@section('title', __('Senior High School Programs'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/education/education-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Education</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-10">
				<h1>Senior High School Programs</h1>

					<p>We offer various courses in the Academic and TVL tracks. See the complete listing below to discover the perfect course for you at ICI.</p>

					<div class="visible-sm visible-xs" id="top">
						<div class="form-group">
						    <span class="select-arrow"><select class="form-control" id="exampleFormControlSelect1">
						      <option>Select course</option>
						      <optgroup label="Iligan">
							      <optgroup label="Academic Track">
							      <option value="ilg-stem">STEM</option>
							      <option value="ilg-abm">ABM</option>
							      <option value="ilg-humss">HUMSS</option>
							      <option value="ilg-gas">GAS</option>
							    </optgroup>
							    <optgroup label="Technical-Vocational-Livelihood Track">
							      <option value="ilg-ce">Computer Engineering</option>
							      <option value="ilg-ee">Electrical Installation and Maintenance</option>
							      <option value="ilg-it">Information Technology</option>
							      <option value="ilg-ca">Culinary Arts</option>
							      <option value="ilg-hrm">Hotel and Restaurant Management</option>
							      <option value="ilg-pn">Practical Nursing</option>
							      <option value="ilg-om">Office Management</option>
							    </optgroup>
							   </optgroup>
							     <optgroup label="CDO">
								      <optgroup label="Academic Track">
								      <option value="cdo-abm">ABM</option>
								      <option value="cdo-humss">HUMSS</option>
								      <option value="cdo-gas">GAS</option>
								    </optgroup>
							    	<optgroup label="Technical-Vocational-Livelihood Track">
								      <option value="cdo-ce">Computer Engineering</option>
								      <option value="cdo-it">Information Technology</option>
								      <option value="cdo-ca">Culinary Arts</option>
								     
								    </optgroup>
							   </optgroup>
							   <optgroup label="Kapatagan">
							    	<optgroup label="Technical-Vocational-Livelihood Track">
								      <option value="kap-ce">Computer Engineering</option>
							      <option value="kap-at">Accounting Technology</option>
							      <option value="kap-it">Information Technology</option>
							      <option value="kap-ca">Culinary Arts</option>
							      <option value="kap-hm">Hotel Management</option>
							      <option value="kap-om">Office Management</option>
								    </optgroup>
							   </optgroup>
						    </select></span>
						  </div>
					</div>

					<h3 class="mb-4 mt-4 c-primary">Iligan</h3>

					<h4>Academic Track</h4>

					<div class="row pt-3">
						<div class="col-md-6 pl-0 mb-4 first" id="ilg-stem">
							<img src="{{asset('img/education/shs-ilg-stem.jpg')}}">

							<h4 class="pt-3 font-regular">STEM</h4>
							<h6>Science, Technology, Engineering and Mathematics</h6>

							<p>This course prepares students for college and graduate studies in the fields of science, technology, engineering, and mathematics. In addition to subject-specific learning, this course aims to foster inquiring minds, logical reasoning, and collaboration skills. </p>

						<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
						      			<h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BA Communication</p>
								        <p class="mb-0">BA Social Studies</p>
								        <p class="mb-0">BA Philosophy</p>
								        <p class="mb-0">BA Political Science</p>
								        <p class="mb-0">BA Elementary Education</p>
								        <p class="mb-0">BA Secondary Education</p>
								        <p class="mb-3">BA Criminology</p>

								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Tutor</p>
								        <p class="mb-0">Teacher Assistant</p>
								        <p class="mb-0">Researcher</p>
								        <p class="mb-0">Training Assistant</p>
								        <p class="mb-0">Community Development Worker</p>
								        <p class="mb-0">Day Care Center Worker</p>
								    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="ilg-abm">
							<img src="{{asset('img/education/shs-abm.jpg')}}">

							<h4 class="pt-3 font-regular">ABM</h4>
							<h6>Accountancy, Business, and Management</h6>

							<p>This course focuses on the foundational concepts of corporate operations, financial management, and business management. You will be trained to analyze assets, understand financial positions, interpret various profitability, and prepare audit accounts.</p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#abm" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="abm" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
						      			<h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Accountancy</p>
								        <p class="mb-0">BS/AB Economics</p>
								        <p class="mb-0">BS Business Administration</p>
								        <p class="mb-0">BS Marketing</p>
								        <p class="mb-3">BS Entrepreneurship</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Accountant</p>
								        <p class="mb-0">Financial Analyst</p>
								        <p class="mb-0">Entrepreneur</p>
								        <p class="mb-0">Marketer</p>
								        <p class="mb-0">Sales Manager</p>
								        <p class="mb-0">Banker</p>
								        <p class="mb-0">Auditor</p>
								    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div>
 -->
						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 first" id="ilg-humss">
							<img src="{{asset('img/education/shs-humss.jpg')}}">

							<h4 class="pt-3 font-regular">HUMSS</h4>
							<h6>Humanities and Social Sciences</h6>

							<p>This strand focuses on the study of societal issues that provides a deeper understanding on the interplay of the different aspects of society. It aims to improve student’s reading, writing, and communication skills. </p>



						<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
						      			<h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BA Communication</p>
								        <p class="mb-0">BA Social Studies</p>
								        <p class="mb-0">BA Philosophy</p>
								        <p class="mb-0">BA Political Science</p>
								        <p class="mb-0">BA Elementary Education</p>
								        <p class="mb-0">BA Secondary Education</p>
								        <p class="mb-3">BA Criminology</p>

								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Tutor</p>
								        <p class="mb-0">Teacher Assistant</p>
								        <p class="mb-0">Researcher</p>
								        <p class="mb-0">Training Assistant</p>
								        <p class="mb-0">Community Development Worker</p>
								        <p class="mb-0">Day Care Center Worker</p>
								    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 third" id="ilg-gas">
							<img src="{{asset('img/education/shs-gas.jpg')}}">

							<h4 class="pt-3 font-regular">GAS</h4>
							<h6>General Academic Strand</h6>

							<p>This course takes on a generalist approach in preparing students for college. Since GAS does not specialize in any strand, you are free to choose any college degree program under the three other strands based on your electives. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#gas" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="gas" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
					      			<h6 class="">What college courses can I take after SHS?</h6>
							        <p class="mb-3">You are free to choose any college degree program under the three other strands based on the elective you chose</p>
							    	<h6 class="">What are my possible professions?</h6>
							    	<p class="mb-0">You can choose any possible profession from the other three strands. However, your chosen profession will likely depend on the specialized subjects, especially the electives, you decide to study. </p>
							    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
					</div>

					<h4 class="mt-5">Technical-Vocational-Livelihood Track</h4>

					<div class="row pt-3">
						<div class="col-md-6 pl-0 mb-4 first" id="ilg-ce">
							<img src="{{asset('img/education/shs-ilg-ce.jpg')}}">

							<h4 class="pt-3 font-regular">Computer Engineering</h4>
							<p class=""><span class="font-bold">Qualification</span> Computer Systems Servicing NC II</p>

							<p>This course focuses on the study of theoretical and practical knowledge of computer hardware necessary for troubleshooting hardware, software, and network system problems.</p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#ce-1" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="ce-1" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Computer Engineering</p>
								        <p class="mb-0">BS Information Technology</p>
								        <p class="mb-0">BS Computer Science</p>
								        <p class="mb-0">BS Information Systems</p>
								        <p class="mb-3">BS Computer Engineering Technology</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">IT Technician</p>
								        <p class="mb-0">Network Engineer</p>
								        <p class="mb-0">Systems Analyst</p>
								        <p class="mb-0">Database Administrator</p>
								        <p class="mb-0">Support Specialist</p>
								        <p class="mb-0">IT Security Specialist</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="ilg-ee">
							<img src="{{asset('img/education/shs-ilg-eim.jpg')}}">

							<h4 class="pt-3 font-regular">Electrical Installation and Maintenance</h4>
							<p class=""><span class="font-bold">Qualification</span> Electrical Installation and Maintenance</p>

							<p>This course focuses on the study of theoretical and practical knowledge of computer hardware necessary for troubleshooting hardware, software, and network system problems.</p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#dma-1" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="dma-1" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Digital Illustration and Animation</p>
								        <p class="mb-0">BS Information Technology</p>
								        <p class="mb-0">BS Entertainment and Multimedia Computing</p>
								        <p class="mb-0">AB Multimedia Arts</p>
								        <p class="mb-0">BFA Visual Arts and Design</p>
								        <p class="mb-0">Associate in Multimedia Technologies</p>
								        <p class="mb-0">Computer-Aided Design and Drafting</p>
								        <p class="mb-3">Associate in Computer Graphics and Animation</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Graphic Designer</p>
								        <p class="mb-0">Art Director/Creative Director</p>
								        <p class="mb-0">UX Designer</p>
								        <p class="mb-0">UI Designer</p>
								        <p class="mb-0">Product Developer</p>
								        <p class="mb-0">Marketing Specialist</p>
								        <p class="mb-0">Multimedia Artist/Animator</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 first" id="ilg-it">
							<img src="{{asset('img/education/shs-ilg-it.jpg')}}">

							<h4 class="pt-3 font-regular">Information Technology</h4>
							<p class=""><span class="font-bold">Qualifications</span> Programming NC IV | <br>Visual Graphic Design NC III</p>

							<p>This course focuses on the study, design, development, implementation, support or management of computer-based information systems. IT workers help ensure that computers work well for people. Students are exposed to scenarios on project management, web design, and customer service scenarios. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#it" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="it" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
					      			<h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Information Technology</p>
								        <p class="mb-0">BS Computer Science</p>
								        <p class="mb-3">BS Information Systems</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">IT Specialist</p>
								        <p class="mb-0">Software Engineer</p>
								        <p class="mb-0">Software Developer</p>
								        <p class="mb-0">Systems Analyst</p>
								        <p class="mb-0">Technical Support</p>
								        <p class="mb-0">Network Engineer</p>
								        <p class="mb-0">Web Developer</p>
							    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div>
 -->
						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a>
 -->
						</div>
						
						
						<div class="col-md-6 pl-0 second" id="ilg-ca">
							<img src="{{asset('img/education/shs-ilg-cul.jpg')}}">

							<h4 class="pt-3 font-regular">Culinary Arts</h4>
							<p class=""><span class="font-bold">Qualifications</span> Food and Beverage Services NC II | Bread and Pastry Production NC II | Cookery NC II</p>

							<p>This course equips students with the practical skills needed for post-secondary education or a career in the food service industry. While mastering culinary techniques, students also learn entrepreneurial concepts, hands-on kitchen experience, and participate in work-based learning experiences.</p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#ca-1" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="ca-1" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Hotel and Restaurant Management</p>
								        <p class="mb-0">BS Hospitality Management</p>
								        <p class="mb-0">BS Food Technology</p>
								        <p class="mb-0">BSEd TLE</p>
								        <p class="mb-3">Culinary Art</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Dessert Specialist</p>
								        <p class="mb-0">Pastry Chef</p>
								        <p class="mb-0">Assistant Chef</p>
								        <p class="mb-0">Personal Chef</p>
								        <p class="mb-0">Prep Cook</p>
								        <p class="mb-0">Line Cook</p>
								        <p class="mb-0">Short Order Cook</p>
								        <p class="mb-0">Restaurant Cook</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 first" id="ilg-hrm">
							<img src="{{asset('img/education/shs-ilg-hr.jpg')}}">

							<h4 class="pt-3 font-regular">Hotel and Restaurant Management </h4>
							<p class=""><span class="font-bold">Qualification</span> Food and Beverage Services NC II | Housekeeping NC II | Bartending NC II</p>

							<p>This course is designed to arm students with knowledge and skills on the proper handling and care of hotel, restaurant, and bar equipment, as well as customers and hotel guests. </p>

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="ilg-pn">
							<img src="{{asset('img/education/shs-ilg-prnr.jpg')}}">

							<h4 class="pt-3 font-regular">Practical Nursing</h4>
							<p class=""><span class="font-bold">Qualification</span> Caregiving NC II</p>

							<p>The course equips trainees with the knowledge, skills and attitudes in nursing in accordance with industry standards. Students learn how to provide care, foster development, and maintain healthy environments. Students are prepared for employment in private and large group physicians' offices, clinics, hospitals, long-term care facilities, and other healthcare areas.</p>

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="ilg-om">
							<img src="{{asset('img/education/shs-ilg-om.jpg')}}">

							<h4 class="pt-3 font-regular">Office Management</h4>
							<p class=""><span class="font-bold">Qualification</span> Events Management Services | Contact Center Services</p>

							<p>This course trains students on the design, implementation, evaluation, and maintenance of the process of work within an office or other organization. Students learn proper planning and control of activities, reduction of office costs, and coordination of all activities of business.</p>

						</div>
					</div>

					<h3 class="mb-4 mt-4 c-primary">CDO</h3>

					<h4>Academic Track</h4>

					<div class="row pt-3">

						<div class="col-md-6 pl-0 mb-4 first" id="cdo-humss">
							<img src="{{asset('img/education/shs-cdo-humms.jpg')}}">

							<h4 class="pt-3 font-regular">HUMSS</h4>
							<h6>Humanities and Social Sciences</h6>

							<p>This course is for people who are curious about what lies on the other side of the fence. In other words, you are prepared to face the world and speak with a large number of people. This is for anybody thinking about majoring in journalism, communication arts, liberal arts, education, or other social science-related fields in college. </p>

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="cdo-abm">
							<img src="{{asset('img/education/shs-cdo-abm.jpg')}}">

							<h4 class="pt-3 font-regular">ABM</h4>
							<h6>Accountancy, Business, and Management</h6>

							<p>This course focuses on developing fundamental accounting, business, and finance abilities that students will need in their professional careers. This course will equip students with the information, skills, and values necessary to become successful accounting professionals and entrepreneurs.</p>
						
						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 third" id="cdo-gas">
							<img src="{{asset('img/education/shs-cdo-gas.jpg')}}">

							<h4 class="pt-3 font-regular">GAS</h4>
							<h6>General Academic Strand</h6>

							<p>While the other courses are focused on a specific career trajectory, this course is ideal for students who are unsure about which path they want to pursue. This route allows you to select electives from a variety of academic strands. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#gas" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="gas" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
					      			<h6 class="">What college courses can I take after SHS?</h6>
							        <p class="mb-3">You are free to choose any college degree program under the three other strands based on the elective you chose</p>
							    	<h6 class="">What are my possible professions?</h6>
							    	<p class="mb-0">You can choose any possible profession from the other three strands. However, your chosen profession will likely depend on the specialized subjects, especially the electives, you decide to study. </p>
							    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
					</div>

					<h4 class="mt-5">Technical-Vocational-Livelihood Track</h4>


					<div class="row pt-3">
						<div class="col-md-6 pl-0 mb-4 first" id="cdo-it">
							<img src="{{asset('img/education/shs-cdo-it.jpg')}}">

							<h4 class="pt-3 font-regular">Information Technology</h4>
							<p class=""><span class="font-bold">Qualifications</span> Programming NC IV | Visual Graphic<br>Design NC III</p>

							<p>You will learn computer principles and key abilities in this course to equip you to communicate and work efficiently in the IT industry. This course will teach you the fundamentals of programming and graphic design, allowing you to become one of the most marketable professionals today.</p>
						</div>

						<div class="col-md-6 pl-0 mb-4 first" id="cdo-ce">
							<img src="{{asset('img/education/shs-cdo-ce.jpg')}}">

							<h4 class="pt-3 font-regular">Computer Engineering</h4>
							<p class=""><span class="font-bold">Qualification</span> Computer Systems Servicing NC II</p>

							<p>Computer engineers seek to increase computers' capabilities and discover new applications for them in other devices and systems. Computer hardware, software, computer architecture, programming, and computer-based systems are all possible areas of study for these engineers. Computer engineering encompasses a variety of occupational titles, including computer hardware engineer, software engineer, and computer architect.</p>
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="cdo-ca">
							<img src="{{asset('img/education/shs-cdo-ca.jpg')}}">

							<h4 class="pt-3 font-regular">Culinary Arts</h4>
							<p class=""><span class="font-bold">Qualifications</span> Bread and Pastry NCII | Food and Beverage Services NCII | Cookery NCII</p>

							<p>This course will teach students about food history, culture, and safety, and the latest food trends. This course is designed to hone cooking abilities and explore opportunities in the broad world of food service through in-depth study and hands-on activities.</p>
						</div>

					</div>

				<h3 class="mb-4 mt-5 c-primary">Kapatagan</h3>

				<h4 class="">Technical-Vocational-Livelihood Track</h4>

				<div class="row pt-3">
						<div class="col-md-6 pl-0 mb-4 first" id="kap-it">
							<img src="{{asset('img/education/shs-kap-it.jpg')}}">

							<h4 class="pt-3 font-regular">Information Technology</h4>
							<p class=""><span class="font-bold">Qualifications</span> Programming NCIV | Visual Graphic Design NCIII</p>

							<p>In this track, you will learn computer concepts and essential skills that will prepare you to communicate and work in today’s fast-paced IT industry. This track is designed to arm you with basic programming and visual design skills that will make you one of the most marketable professionals today. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#it-2" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="it-2" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
					      			<h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Information Technology</p>
								        <p class="mb-0">BS Computer Science</p>
								        <p class="mb-3">BS Information Systems</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">IT Specialist</p>
								        <p class="mb-0">Software Engineer</p>
								        <p class="mb-0">Software Developer</p>
								        <p class="mb-0">Systems Analyst</p>
								        <p class="mb-0">Technical Support</p>
								        <p class="mb-0">Network Engineer</p>
								        <p class="mb-0">Web Developer</p>
							    </div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
						<div class="col-md-6 pl-0 mb-4 second" id="kap-ce">
							<img src="{{asset('img/education/shs-kap-ce.jpg')}}">

							<h4 class="pt-3 font-regular">Computer Engineering</h4>
							<p class=""><span class="font-bold">Qualifications</span> Computer Hardware Servicing NCII | Computer Systems Servicing NCII</p>

							<p>This course provides hands-on computer engineering practice. The program will equip students with the ability to analyze, plan, design, install, operate and maintain digital devices, computer hardware, and software systems.</p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#ce-2" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="ce-2" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Computer Engineering</p>
								        <p class="mb-0">BS Information Technology</p>
								        <p class="mb-0">BS Computer Science</p>
								        <p class="mb-0">BS Information Systems</p>
								        <p class="mb-3">BS Computer Engineering Technology</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">IT Technician</p>
								        <p class="mb-0">Network Engineer</p>
								        <p class="mb-0">Systems Analyst</p>
								        <p class="mb-0">Database Administrator</p>
								        <p class="mb-0">Support Specialist</p>
								        <p class="mb-0">IT Security Specialist</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
						<div class="col-md-6 pl-0 mb-4 first" id="kap-at">
							<img src="{{asset('img/education/shs-kap-at.jpg')}}">

							<h4 class="pt-3 font-regular">Accounting Technology</h4>

							<p>The track is centered on developing basic accounting, business, and finance skills that will help students pursue careers in bookkeeping, auditing, tax preparation, and business analysis. This track is designed to prepare students to become competent professionals in the field of accounting.</p>

						<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#at" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="at" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Secretary</p>
								        <p class="mb-0">Bookkeeper</p>
								        <p class="mb-0">Accountant</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>

						<div class="col-md-6 pl-0 mb-4 second" id="kap-om">
							<img src="{{asset('img/education/shs-kap-office.jpg')}}">

							<h4 class="pt-3 font-regular">Office Management</h4>
							<p class=""><span class="font-bold">Qualifications</span> Events Management NCIII | Bookkeeping NCIII</p> 

							<p>In this track, students will learn how to communicate effectively, resolve conflicts at work, and develop management skills. This track is designed to help students become an efficient worker, one who is able to handle stress and pressure, in the jobs that he/she will want to pursue. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#om" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="om" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Event Coordinator</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
						<div class="col-md-6 pl-0 mb-4 first" id="kap-hm">
							<img src="{{asset('img/education/shs-kap-hm.jpg')}}">

							<h4 class="pt-3 font-regular">Hotel Management</h4>
							<p class=""> <span class="font-bold">Qualifications</span> Front Office NCII | Housekeeping NCII | Food and Beverage Services NCII</p>

							<p>In this track, you will learn front desk and housekeeping skills that will help you become a hospitality pro. Hospitality can be defined in so many ways, but in general it is about providing the best customer experience. </p>

							<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#hm" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="hm" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Front Desk Officer</p>
						        		<p class="mb-0">Bartender</p>
								        <p class="mb-0">Waiter</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
						<div class="col-md-6 pl-0 mb-4 second" id="kap-ca">
							<img src="{{asset('img/education/shs-kap-cul.jpg')}}">

							<h4 class="pt-3 font-regular">Culinary Arts</h4>
							<p class=""><span class="font-bold">Qualifications</span> Bread and Pastry NCII | Food and Beverage Services NCII | Cookery NCII</p>

							<p>In this track, you will learn about food history, food culture, food safety, and current food trends. Through in-depth study in the culinary arts field and hands-on activities, this track will help you hone your cooking skills and give you the opportunity to explore careers in the exciting and vast industry of food service. </p>

						<!-- <div class="accordion " id="specializations">
						  <div class="no-border">
						    <div class="accord-1" id="headingOne">
						        <a class="" data-toggle="collapse" data-target="#ca-2" aria-expanded="false" aria-controls="collapseOne">
						          Learn More
						        </a>
						    </div>

						    <div id="ca-2" class="collapse" aria-labelledby="headingOne" data-parent="#specializations">
						      <div class="card-body ml-3 pb-3">
						      	<div class="row">
						      		<div class="col-md-12 pl-0">
								        <h6 class="">What college courses can I take after SHS?</h6>
								        <p class="mb-0">BS Hotel and Restaurant Management</p>
								        <p class="mb-0">BS Hospitality Management</p>
								        <p class="mb-0">BS Food Technology</p>
								        <p class="mb-0">BSEd TLE</p>
								        <p class="mb-3">Culinary Art</p>
								    	<h6 class="">What are my possible professions?</h6>
								    	<p class="mb-0">Dessert Specialist</p>
								        <p class="mb-0">Pastry Chef</p>
								        <p class="mb-0">Assistant Chef</p>
								        <p class="mb-0">Personal Chef</p>
								        <p class="mb-0">Prep Cook</p>
								        <p class="mb-0">Line Cook</p>
								        <p class="mb-0">Short Order Cook</p>
								        <p class="mb-0">Restaurant Cook</p>
							    	</div>
						      	</div>
						      	
						      </div>
						    </div>
						  </div>
						</div> -->

						<!-- <a href="" class="mt-0 c-magenta font-extrabold">Download Prospectus</a> -->
						</div>
					</div>

				

				

			</div>
			@include('frontend.includes.education-sidebar')
		</div>

		<button onclick="topFunction()" id="myBtn" title="Go to top" class="visible-xs visible-sm hidden-md"><i class="fas fa-arrow-up"></i></button>
	</div>
</section>
@endsection

@push('after-scripts')
<script>
$('select').on('change', function(){
    $('body,html').animate({ scrollTop: $('#' + $(this).val()).offset().top });
});
</script>

<script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	$('body,html').animate({ scrollTop: $('#top').position().top });
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

@endpush