@extends('frontend.layouts.app')

@section('title', __('News'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/news/news-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">News</h1>
      </div>
    </div>
  </div>
</section>

@if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-9">
				<h1>News</h1>

				

				<div class="row pt-3">
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-1.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">ICI is now included in the UP College Admissions Online Application Portal!</h6>
						    <p class="card-text">Grade 12 ICIans: Our school is now included in the UP College Admissions Online Application Portal!</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-2.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">GICI Weekly CASH Pahalipay for all Grade 10 Students in Iligan City</h6>
						    <p class="card-text">Win up to P1,000 Cash! Participants MUST be a GRADE 10 STUDENT in any PUBLIC or PRIVATE school in Iligan City.</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-3.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">1st Batch of Parent-Teacher Conference (Virtual & Limited)</h6>
						    <p class="card-text">Thank you dear parents for your time, rest assured all your concerns will be addressed promptly.</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-1.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">ICI is now included in the UP College Admissions Online Application Portal!</h6>
						    <p class="card-text">Grade 12 ICIans: Our school is now included in the UP College Admissions Online Application Portal!</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-2.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">GICI Weekly CASH Pahalipay for all Grade 10 Students in Iligan City</h6>
						    <p class="card-text">Win up to P1,000 Cash! Participants MUST be a GRADE 10 STUDENT in any PUBLIC or PRIVATE school in Iligan City.</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
					<div class="col-md-4 first mb-4">
						<div class="card">
						  <img src="{{asset('img/n-3.jpg')}}" class="card-img-top" alt="...">
						  <div class="card-body p-3">
						    <h6 class="card-title">1st Batch of Parent-Teacher Conference (Virtual & Limited)</h6>
						    <p class="card-text">Thank you dear parents for your time, rest assured all your concerns will be addressed promptly.</p>
						    <a href="#" class="c-primary font-bold">Read more</a>
						  </div>
						</div>
					</div>
				</div>

				
			</div>

			<div class="col-md-3">
				<div class="bg-secondary p-4">
				<!-- Search widget-->
				<h5 class="mb-3 ">Search</h5>
				<div class="input-group">
                    <input class="form-control" type="text" placeholder="Search & enter" />
                    <!-- <span class="input-group-append"><button class="btn btn-secondary" type="button">Go!</button></span> -->
                </div>
                <hr class="mt-4 mb-4">
                  <!-- Categories widget-->
                  <h5 class="mb-3 mt-3 ">Categories</h5>
	                  <ul class="list-unstyled mb-0">
	                    <li><a href="#!">Institute News</a></li>
	                    <li><a href="#!">Institute Events</a></li>
	                    <li><a href="#!">Talks/Webinars</a></li>
	                    <li><a href="#!">Awards/Recognitions</a></li>
	                    <li><a href="#!">CSR Initiatives</a></li>
	                </ul>

	                <hr class="mt-4 mb-4">
                  <!-- Categories widget-->
                  <h5 class="mb-3 mt-3 ">Recent Posts</h5>
                  <div>
	                <p class="mb-0"><a href="#!" class="c-bod">ICI is now included in the UP College Admissions Online Application Portal!</a></p>
	                <small>Jan 23, 2021</small>
	                <hr>
	                <p class="mb-0"><a href="#!" class="c-bod">GICI Weekly CASH Pahalipay for all Grade 10 Students in Iligan City</a></p>
	                <small>Feb 14, 2021</small>
	                <hr>
	                 <p class="mb-0"><a href="#!" class="c-bod">1st Batch of Parent-Teacher Conference (Virtual & Limited)</a></p>
	                <small>Mar 06, 2021</small>
	                </div>

	              <hr class="mt-4 mb-4">
                  <!-- Categories widget-->
                  <h5 class="mb-3 mt-3 ">Archives</h5>
	                  <ul class="list-unstyled mb-0">
	                    <li><a href="#!">April 2021</a></li>
	                    <li><a href="#!">March 2021</a></li>
	                    <li><a href="#!">February 2021</a></li>
	                    <li><a href="#!">January 2021</a></li>
	                    <li><a href="#!">December 2020</a></li>
	                </ul>
	            </div>
			</div>
		</div>
	</div>
</section>

@endsection