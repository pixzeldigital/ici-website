@extends('frontend.layouts.app')

@section('title', __('Student Life'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/student-life/student-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Student Life</h1>
      </div>
    </div>
  </div>
</section>

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10">

				<div id="holistic-growth">
					<h1>Holistic Growth</h1>

					<div class="row">

						<div class="col-md-12 pt-4 pl-0 second">

							<img src="{{asset('img/student-life/holistic-growth.jpg')}}" >

							<div class="col-lg-9 col-md-12 bg-primary p-3 card-holistic">
								<p class="lead mb-0 font-semibold c-white">At ICI, we aim to nurture the holistic growth of our students with activities that further develop their skills and promote healthy camaraderie within the student body. </p>
								
							</div>

						</div>

						<div class="col-md-12 pl-0 first">

							<h4 class="mb-3 mt-4 c-blue">Well-Rounded Individuals</h4>

							

							<p class="">Our students are well-rounded individuals that thrive inside and outside the classroom.</p>

							<p class="">We want our students to excel not only in their academic pursuits but also in their personal goals. ICI encourages its students to explore their interests and discover their passions through various clubs. </p>

							<p class="mt-3 mb-3">We believe that cultivating an environment where students learn in multiple ways aids them in becoming empowered adults.  </p>
						</div>

						
						
					</div>

							

					<div class="row">

							<!-- <div class="col-md-4 pl-0 first">
								<img src="{{asset('img/student-life/holistic.jpg')}}">
							</div> -->

							<div class="col-md-6 pl-0 second">
								<img src="{{asset('img/student-life/student-life.jpg')}}">
							</div>

							<div class="col-md-6 pl-0 third">
								<img src="{{asset('img/student-life/student-life-2.jpg')}}">
							</div>

						</div>
				</div>

				<div id="extracurriculars" class="mt-5">
					<h1>Extracurriculars for Students</h1>

					<p class="mt-4 mb-5 first">Our students get to participate in several extracurricular activities throughout the school year. </p>

					<div class="row">

						<div class="col-md-6 pl-0 first">

							<h5 class="mb-3 c-blue">Acquaintance Party</h5>

							<p class="">ICI welcomes the school year through its Acquaintance Party. Students get to enjoy a night of performances, music, and dancing. </p>

						</div>

						<div class="col-md-6 pl-0 second">


							<h5 class="mb-3 c-blue">Student Government Elections</h5>

							<p class="">Students get to exercise their right to vote as constituents of ICI. The elections also give students the opportunity to hone their leadership skills.</p>
						</div>
					</div>

					<div class="row pt-3">

						<div class="col-md-6 pl-0 first">

							<h5 class="mb-3 c-blue">Clubs</h5>

							<p class="">A variety of clubs are available to students so that they can pursue their interests and uncover their passions outside their classes.</p>

						</div>

						<div class="col-md-6 pl-0 second">
							<h5 class="mb-3 c-blue">Skills Olympics</h5>

							<p class="">The biggest event of the school year where students showcase their skills through different competitions.</p>
						</div>
					</div>
					
					


					
				</div>

				<div id="atici" class="mt-5">
					<h1>Student Life at ICI</h1>

				<div class="row pt-4"><div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-6.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-6.jpg')}}" ></a>
					</div>
					
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-12.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-12.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-1.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-1.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-2.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-2.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-3.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-3.jpg')}}" ></a>
					</div>
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-5.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-5.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-4.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-4.jpg')}}" ></a>
						<a href="{{asset('/img/student-life/atici-9.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-9.jpg')}}" ></a>
					</div>
					
					
					
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-10.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-10.jpg')}}" ></a>
					</div>
					<div class="col-md-8 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-11.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-11.jpg')}}" ></a>
					</div>
					<!-- <div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/student-life/atici-7.jpg')}}" data-lightbox="ilg" data-title="Student Life"><img src="{{asset('/img/student-life/atici-7.jpg')}}" ></a>
					</div> -->
				</div>
				</div>

				
			</div>
			@include('frontend.includes.studentlife-sidebar')
		</div>
	</div>
</section>

@endsection