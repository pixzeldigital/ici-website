@extends('frontend.layouts.app')

@section('title', __('Student Life at ICI'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/student-life/student-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Student Life</h1>
      </div>
    </div>
  </div>
</section>

@if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			@include('frontend.includes.studentlife-sidebar')
			<div class="col-md-9">
				<h1>Student Life at ICI</h1>

				<div class="masonry">
				  <div class="item"><a href="{{asset('/img/student-life/atici-1.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-1.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-3.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-3.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-2.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-2.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-4.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-4.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-5.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-5.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-6.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-6.jpg')}}"></a></div>
				   <div class="item"><a href="{{asset('/img/student-life/atici-7.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-7.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-8.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-8.jpg')}}"></a></div>
				  <div class="item"><a href="{{asset('/img/student-life/atici-9.jpg')}}" data-lightbox="student-life" data-title="Student Life"><img src="{{asset('/img/student-life/atici-9.jpg')}}"></a></div>
				</div>
				
			</div>
		</div>
	</div>
</section>

@endsection