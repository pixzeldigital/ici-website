@extends('frontend.layouts.app')

@section('title', __('Holistic Growth'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/student-life/student-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Student Life</h1>
      </div>
    </div>
  </div>
</section>

@if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			@include('frontend.includes.studentlife-sidebar')
			<div class="col-md-9">
				<h1>Holistic Growth</h1>

				<p class="mt-4 first">At ICI, we aim to nurture the holistic growth of our students with activities that further develop their skills and promote healthy camaraderie within the student body. </p>

				<h5 class="mb-3 mt-5 c-blue">Well-Rounded Individuals</h5>

				<div class="row">

					<div class="col-md-7 pl-0">
						

						<p class=" first">Our students are well-rounded individuals that thrive inside and outside the classroom.</p>

						<p class=" first">We want our students to excel not only in their academic pursuits but also in their personal goals. ICI encourages its students to explore their interests and discover their passions through various clubs. </p>

						<p class=" first">We believe that cultivating an environment where students learn in multiple ways aids them in becoming empowered adults.  </p>
					</div>

					<div class="col-md-5">

						<img src="{{asset('img/student-life/holistic-growth.jpg')}}">

					</div>
					
				</div>

				<img src="{{asset('img/student-life/holistic-growth-2.jpg')}}">


				

				
			</div>
		</div>
	</div>
</section>

@endsection