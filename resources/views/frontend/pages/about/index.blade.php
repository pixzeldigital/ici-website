@extends('frontend.layouts.app')

@section('title', __('About ICI'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/about-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">About ICI</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif
 -->
<section class="space--xs" >
	<div class="container">
		<div class="row p-0">
			<div class="col-md-10">

				<div id="ataglance">
				<h1 >At a Glance</h1>

				<p class="first">Since its inception in 1997, ICI has become one of the leading providers of technical education and more recently, secondary education, in Iligan City and its neighboring towns. </p>

				<p class="mb-5 first">ICI focuses on providing TESDA Courses and Senior High Courses to the Filipino youth, equipping them with skills and knowledge to succeed in the real world. </p>

				<div class="row">
					<div class="col-md-3 bg-primary p-3 first">
						<h1 class="mb-0 c-white">3</h1>
						<h5 class="font-semibold c-white">Locations in Northern Mindanao</h5>
					</div>
					<div class="col-md-3 bg-secondary-2 p-3 second">
						<h1 class="mb-0 c-white">24</h1>
						<h5 class="font-semibold c-white">Years of Quality Education</h5>
					</div>
					<div class="col-md-3 bg-secondary-3 p-3 third">
						<h1 class="mb-0 c-white">100+</h1>
						<h5 class="font-semibold c-white">Faculty & Staff</h5>
					</div>
					<div class="col-md-3 bg-gray p-3 fourth">
						<h1 class="mb-0 c-white">5000+</h1>
						<h5 class="font-semibold c-white">Total SHS Student Population</h5>
					</div>
				</div>

				<!-- history -->
				<div id="history">
				<h1 class="mt-5 mb-4">History</h1>
				<p class="first"><span class="font-bold">Iligan Computer Institute</span> (stylized as ICI) formally opened at the Diocesan Centrum on April 4, 1997. It started its operations with its first 4 courses: Computer Programming, Computer Technician, Computer Secretarial, and Desktop Publishing. The first batch consisted of 60 students and had their classes in the 3 rented rooms the school currently had. In March 1998, ICI produced its first batch of graduates.</p>

				<div class="row pb-4">
					<div class="col-md-6 pl-0 first">
						<div class="card no-border p-3 bg-primary text-white">
							  	<h1 class="mb-0 c-white title">50</h1>
							 <h4 class="c-white mb-0">Graduates in 1998</h4>
							  
							</div>
						</div>
					<div class="col-md-6 pl-0 second">
						<div class="card no-border p-3 bg-secondary-2 text-white">
							  
							  	<h1 class="mb-0 c-white title">25,000+</h1>
							 <h4 class="c-white mb-0">Total Graduates as of 2020</
							  </h4>
						</div>
					</div>
				</div>

				<p class="first">In the years that followed, 8 new courses were offered namely: Network Engineering Technology, Information Technology, Computer Engineering Technology, Computer Science Technology, Hardware Technician, Systems Programming, Computer Encoder, Computer Drafting. </p>

				<div class="row">
					<div class="col-md-6 pl-0 first">
						<p class="h4 font-bold c-black">From the beginning, the school has always believed in a <span class="c-primary">hands-on approach</span> to learning. This gives its students an edge in the world of employment. </p>
					</div>

					<div class="col-md-6 pl-0 second">
						<p class="">ICI soon ventured into offering programs in Nursing, Caregiving, HRM, and Animation. The school also expanded to Cagayan de Oro and Kapatagan. In 2016, ICI grew further with its Senior High School curriculum, offering various strands in the Academic, TVL, and GAS tracks. </p>

					</div>
				</div>

				<div class="row pt-3">
					<div class="col-md-7 pl-0 mb-3 first">
						<img src="{{asset('/img/about/location.jpg')}}">
					</div>

					<div class="col-md-5 pl-0 second">
						<p class="h4 font-bold c-black mb-5">Today, ICI continues to progress with the development of its own school buildings in <span class="c-primary">Cagayan de Oro</span> and <span class="c-primary">Kapatagan</span>, and a school campus in <span class="c-primary">Iligan City</span> on the horizon.</p>

					</div>
				</div>
			</div>
				
			<!-- mission -->

			<div id="mission" class="mt-5">

				<h1 class="mb-4">ICI's Mission</h1>

				<div class="card hidden-xs hidden-sm first">
				  <img src="{{asset('img/about/mission-1.jpg')}}" class="card-img" alt="...">
				  <div class="card-img-overlay text-center">
				    <p class="quote font-semibold mb-0">To produce competent students for the workforce by providing relevant academic programs conducted by certified instructors using industry standard equipment and facilities.</p>
				  </div>
				</div>

				<div class="card hidden-lg hidden-md">

					<img src="{{asset('img/about/mission-1.jpg')}}" class="" alt="">

					<p class="quote mt-1 mb-0 text-center font-bold">To produce competent students for the workforce by providing relevant academic programs conducted by certified instructors using industry standard equipment and facilities.</p>

				</div>

			</div>

			<div id="locations" class="mt-5">

				<h1 class="mb-4">Locations</h1>

				<!-- <iframe src="https://snazzymaps.com/embed/310431" width="100%" height="400px" style="border:none;"></iframe> -->
				<img src="{{asset('img/about/map.jpg')}}" class="first">
				<img src="{{asset('img/about/map-1.jpg')}}" class="mt-3 first">

				<div class="row pt-3">
					<div class="col-md-4 first">
						<div class="d-flex mb-3">
							<div><i class="fas fa-map-marker-alt c-magenta mr-3"></i></div>
							<div>
								<h5 class="font-bold">Kapatagan</h5>
	                        </div>
						</div>
						
					</div>
					<div class="col-md-4 second">
						<div class="d-flex mb-3">
							<div><i class="fas fa-map-marker-alt c-primary mr-3"></i></div>
							<div>
								<h5 class="font-bold">Iligan</h5>
	                        </div>
						</div>
					</div>
					<div class="col-md-4 third">
						<div class="d-flex mb-3">
							<div><i class="fas fa-map-marker-alt c-blue mr-3"></i></div>
							<div>
								<h5 class="font-bold">Cagayan de Oro</h5>
	                        </div>
						</div>
					</div>
				</div>

			</div>

			<div id="leadership" class="mt-5">

				<h1 class="mb-4">Leadership</h1>

				<div class="row pt-3">
					<div class="col-md-5 pl-0 first">
						<img src="{{asset('img/about/tabanao.jpg')}}">
					</div>

					<div class="col-md-6 second">
						<h4 class="font-semibold mt-2">Ladislao C. Tabanao Jr.</h4>
						<h5 class="font-regular c-primary">Founder and President</h5>
					</div>
				</div>
				<div class="row pt-4">
					<div class="col-md-3 pl-0 first">
						<img src="{{asset('img/about/suaring.jpg')}}">
						<h5 class="font-semibold mt-4">Jerrum M. Suaring</h5>
						<h6 class="font-regular c-primary">VP for Operations</h6>
					</div>
					<div class="col-md-3 pl-0 second">
						<img src="{{asset('img/about/flores.jpg')}}">
						<h5 class="font-semibold mt-4">Vernabeth L. Flores, LPT</h5>
						<h6 class="font-regular c-primary">SHS Principal</h6>
					</div>
					<div class="col-md-3 pl-0 third">
						<img src="{{asset('img/about/tabanao-1.jpg')}}">
						<h5 class="font-semibold mt-4">Rio F. Tabanao</h5>
						<h6 class="font-regular c-primary">CDO Branch Manager</h6>
					</div>
					<div class="col-md-3 pl-0 fourth">
						<img src="{{asset('img/about/piedad.jpg')}}">
						<h5 class="font-semibold mt-4">Dinna Lou J. Piedad</h5>
						<h6 class="font-regular c-primary">Kapatagan Branch Manager</h6>
					</div>
				</div>

			</div>

			<div id="faculty-staff" class="mt-5">

				<h1 class="mb-4">Faculty & Staff</h1>

				<div class="hidden-lg hidden-md">
					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner">
					    <div class="carousel-item active">
					      <img src="{{asset('/img/about/f-1.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-2.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-12.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-15.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-5.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-6.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-7.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-8.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-13.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-10.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-11.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-3.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-9.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-14.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					    <div class="carousel-item">
					      <img src="{{asset('/img/about/f-4.jpg')}}" class="d-block w-100" alt="...">
					    </div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>
				</div>

				<div class="row pt-4 hidden-xs hidden-sm">
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-1.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-1.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-2.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-2.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-12.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-12.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-15.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-15.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-5.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-5.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-6.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-6.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-7.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-7.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-8.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-8.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-13.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-13.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-10.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-10.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-11.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-11.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-3.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-3.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-9.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-9.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-14.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-14.jpg')}}" ></a>
					</div>
					<div class="col-md-4 pl-0 pr-0">
						<a href="{{asset('/img/about/f-4.jpg')}}" data-lightbox="ilg" data-title="Faculty & Staff"><img src="{{asset('/img/about/f-4.jpg')}}" ></a>
					</div>
				</div>

			</div>
			

				<!-- <div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/gallery.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Gallery</h3>
						<p>ICI is rendering its services at 3 developing cities in Northern Mindanao.</p>

						<a href="/about/gallery">Read More</a>
					</div>
				</div> -->
			</div>
			</div>
			@include('frontend.includes.about-sidebar')
		</div>
	</div>
</section>



@endsection