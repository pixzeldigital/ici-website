@extends('frontend.layouts.app')

@section('title', __('About ICI'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/about-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">About ICI</h1>
      </div>
    </div>
  </div>
</section>

@if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			@include('frontend.includes.about-sidebar')
			<div class="col-md-9">
				<h1>At a Glance</h1>
				<h4 class="c-primary font-semibold mb-4">Your Skills. Your Success.</h4>

				<p class="first">Since its inception in 1997, ICI has become one of the leading providers of technical education and more recently, secondary education, in Iligan City and its neighboring towns. </p>

				<p class="mb-5 first">ICI focuses on providing TESDA Courses and Senior High Courses to the Filipino youth, equipping them with skills and knowledge to succeed in the real world. </p>

				<div class="row">
					<div class="col-md-3 bg-primary p-3 first">
						<h1 class="mb-0 c-white">3</h1>
						<h5 class="font-semibold c-white">Locations in Northern Mindanao</h5>
					</div>
					<div class="col-md-3 bg-secondary-2 p-3 second">
						<h1 class="mb-0 c-white">24</h1>
						<h5 class="font-semibold c-white">Years of Quality Education</h5>
					</div>
					<div class="col-md-3 bg-secondary-3 p-3 third">
						<h1 class="mb-0 c-white">100+</h1>
						<h5 class="font-semibold c-white">Faculty & Staff</h5>
					</div>
					<div class="col-md-3 bg-gray p-3 fourth">
						<h1 class="mb-0 c-white">2k+</h1>
						<h5 class="font-semibold c-white">Total SHS Student Population</h5>
					</div>
				</div>

				<div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/hero-1.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">History</h3>
						<p>Iligan Computer Institute (stylized as ICI) formally opened at the Diocesan Centrum on April 4, 1997. It started its operations with its first 4 courses: Computer Programming, Computer Technician, Computer Secretarial, and Desktop Publishing.</p>

						<a href="/about/history">Read More</a>
					</div>
				</div>
				
				<div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/mission.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Mission</h3>
						<p>To produce competent students for the workforce by providing relevant academic programs conducted by certified instructors using industry standard equipment and facilities.</p>

						<a href="/about/mission">Read More</a>
					</div>
				</div>

				<div class="row pt-5 align-c firstontent-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/location.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Locations</h3>
						<p>ICI is rendering its services at 3 developing cities in Northern Mindanao.</p>

						<a href="/about/locations">Read More</a>
					</div>
				</div>

				<div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/leadership.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Leadership</h3>
						<p>ICI is rendering its services at 3 developing cities in Northern Mindanao.</p>

						<a href="/about/leadership">Read More</a>
					</div>
				</div>

				<div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/faculty.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Faculty & Staff</h3>
						<p>With over 200+ faculty and 100+ staff, ICI is rendering its services with reliable and qualified educators.</p>

						<a href="/about/faculty-staff">Read More</a>
					</div>
				</div>

				<div class="row pt-5 align-content-center">
					<div class="col-md-5 p-0 first">
						<img src="{{asset('/img/about/gallery.jpg')}}">
					</div>

					<div class="col-md-7 pl-3 second">
						<h3 class="c-secondary-2">Gallery</h3>
						<p>ICI is rendering its services at 3 developing cities in Northern Mindanao.</p>

						<a href="/about/gallery">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection