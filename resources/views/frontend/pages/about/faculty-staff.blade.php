@extends('frontend.layouts.app')

@section('title', __('Faculty & Staff'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/about-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">About ICI</h1>
      </div>
    </div>
  </div>
</section>

@if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			@include('frontend.includes.about-sidebar')
			<div class="col-md-9">
				<h1 class="mb-4">Faculty & Staff</h1>


				<h4 class="mb-4 c-primary">Iligan Branch</h4>
				<div class="d-flex ">
					<div class="col">
						<a href="{{asset('/img/about/ilg-1.jpg')}}" data-lightbox="ilg" data-title="Iligan Branch"><img src="{{asset('/img/about/ilg-1.jpg')}}" ></a>
					</div>
					<div class="col">
						<a href="{{asset('/img/about/ilg-2.jpg')}}" data-lightbox="ilg" data-title="Iligan Branch"><img src="{{asset('/img/about/ilg-2.jpg')}}"></a>
					</div>
					<div class="col">
						<a href="{{asset('/img/about/ilg-3.jpg')}}" data-lightbox="ilg" data-title="Iligan Branch"><img src="{{asset('/img/about/ilg-3.jpg')}}"></a>
					</div>
				</div>

				<h4 class="mb-4 c-primary mt-5">Kapatagan Branch</h4>
				<div class="d-flex">
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
				</div>

				<h4 class="mb-4 c-primary mt-5">CDO Branch</h4>
				<div class="d-flex">
					<div class="col">
						<a href="{{asset('/img/about/cdo-1.jpg')}}" data-lightbox="cdo" data-title="CDO Branch"><img src="{{asset('/img/about/cdo-1.jpg')}}" ></a>
					</div>
					<div class="col">
						<a href="{{asset('/img/about/cdo-2.jpg')}}" data-lightbox="cdo" data-title="CDO Branch"><img src="{{asset('/img/about/cdo-2.jpg')}}"></a>
					</div>
					<div class="col">
						<a href="{{asset('/img/about/cdo-3.jpg')}}" data-lightbox="cdo" data-title="CDO Branch"><img src="{{asset('/img/about/cdo-3.jpg')}}"></a>
					</div>
				</div>

				<!-- <h4 class="mb-4 c-primary mt-5">Culinary Arts Department Branch</h4>
				<div class="d-flex">
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
					<div class="col">
						<img src="/img/hero-1.jpg">
					</div>
				</div> -->
			</div>
		</div>
	</div>
</section>
@endsection