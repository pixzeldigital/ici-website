@extends('frontend.layouts.app')

@section('title', __('Admissions'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/admissions/admissions-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Admissions</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif -->

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10 mt-3">
				<div class="row">
					<div class="col-lg-9 col-md-12 pl-0">
						<h1 class="mb-4">Senior High School Admissions</h1>
					</div>
					<div class="col-lg-3 col-md-12 mb-5 pl-0">
						<a href="/education/shs" class="btn btn-primary text-right">View SHS Courses</a>
					</div>
				</div>
				<div id="shs">
					<ul class="nav nav-pills mb-4 flex-column flex-sm-row" id="pills-tab" role="tablist">
					<li class="nav-item flex-sm-fill text-center">
					    <a class="nav-link active " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Are you an Incoming Grade 11 Student?</a>
					  </li>
					  <li class="nav-item flex-sm-fill text-center">
					    <a class="nav-link " id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Are you an Incoming Grade 12 Student?</a>
					  </li>
				</ul>
			</div>
				<!-- <p class="mb-5"><span class="font-bold">Iligan Computer Institute</span> (stylized as ICI) formally opened at the Diocesan Centrum on April 4, 1997. It started its operations with its first 4 courses: Computer Programming, Computer Technician, Computer Secretarial, and Desktop Publishing. The first batch consisted of 60 students and had their classes in the 3 rented rooms the school currently had. In March 1998, ICI produced its first batch of graduates.</p> -->

				<!-- <nav class="nav nav-pills nav-justified mb-4">
				  <a class="nav-item nav-link active" href="#">New Student</a>
				  <a class="nav-item nav-link" href="#">Old Student</a>
				</nav> -->
				<div class="tab-content" id="pills-tabContent">
  					<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
				
				<div class="row">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">1</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Know your classification. Requirements for enrollment will depend on your particular enrollee classification. Be sure to know which category you belong to.</p>

						<div class="shs-1 justify-content-around">
							<div>
								<div class="card no-border text-center">
								  <img src="{{asset('/img/admissions/public.png')}}" class="card-img" alt="...">
								
								 <h6 class="mt-3 mb-0">Grade 10 Graduate from Public School</h6>

								</div>
							</div>
							<div>
								<div class="card no-border text-center">
								  <img src="{{asset('/img/admissions/private.png')}}" class="card-img" alt="...">
								 <h6 class="mt-3">Grade 10 Graduate from Private School</h6>
								</div>
							</div>
							<div>
								<div class="card no-border text-center">
								  <img src="{{asset('/img/admissions/old.png')}}" class="card-img" alt="...">
								 <h6 class="mt-3">Old Curriculum Students (Not included in K12)</h6>
								</div>
							</div>
							<div class="align-self-stretch">
								<div class="card no-border text-center">
								  <img src="{{asset('/img/admissions/transferee.png')}}" class="card-img" alt="...">
								 <h6 class="mt-3">Transferee from<br>other school</h6>
								</div>
							</div>
							<div>
								<div class="card no-border text-center">
								  <img src="{{asset('/img/admissions/als.png')}}" class="card-img" alt="...">
								 <h6 class="mt-3">Alternative Learning System (ALS)</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row pt-5">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">2</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Prepare the necessary requirements for enrollment. </p>

						<div class="col-md-12 d-flex align-items-stretch">

							<div class="tab-page">
							  <button class="tablinks active" onclick="openCat(event, 'c1')">Grade 10 Graduate from Public School</button>
							  <button class="tablinks" onclick="openCat(event, 'c2')">Grade 10 Graduate from Private School</button>
							  <button class="tablinks" onclick="openCat(event, 'c3')">Old Curriculum Students<br>(Not included in K12)</button>
							  <button class="tablinks" onclick="openCat(event, 'c4')">Transferee</button>
							  <button class="tablinks" onclick="openCat(event, 'c5')">ALS</button>
							</div>

							<div id="c1" class="tabpage-content" style="display: block;">
							  <h5 class="mb-3">Grade 10 Graduate from<br>Public School</h5>
							  <p class="mb-1">Form 137</p>
									<p class="mb-1">Form 138</p>
									<p class="mb-1">Birth Certificate</p>
									<p class="mb-1">Certificate of Good Moral Character</p>
									<p class="mb-1">Diploma or Certificate of Completion</p>
							</div>

							<div id="c2" class="tabpage-content">
							  <h5 class="mb-3">Grade 10 Graduate from<br>Private School</h5>
							  <p class="mb-1">Form 137</p>
									<p class="mb-1">Form 138</p>
									<p class="mb-1">Birth Certificate</p>
									<p class="mb-1">Certificate of Good Moral Character</p>
									<p class="mb-1">Diploma or Certificate of Completion</p>
									<p class="mb-1">ESC Certificate or FAPE</p>
							</div>

							<div id="c3" class="tabpage-content">
							  <h5 class="mb-3">Old Curriculum Students<br>(Not included in K12)</h5>
							  <p class="mb-1">Form 137</p>
									<p class="mb-1">Form 138</p>
									<p class="mb-1">Birth Certificate</p>
									<p class="mb-1">Certificate of Good Moral Character</p>
									<p class="mb-1">Diploma</p>
							</div>
							<div id="c4" class="tabpage-content">
							  <h5 class="mb-3">Transferee</h5>
							  <p class="mb-1">Form 137</p>
									<p class="mb-1">Form 138</p>
									<p class="mb-1">Birth Certificate</p>
									<p class="mb-1">Certificate of Good Moral Character</p>
							</div>
							<div id="c5" class="tabpage-content">
							  <h5 class="mb-3">ALS</h5>
							  <p class="mb-1">Form 137</p>
									<p class="mb-1">Form 138</p>
									<p class="mb-1">Birth Certificate</p>
									<p class="mb-1">Certificate of Good Moral Character</p>
									<p class="mb-1">Diploma</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row pt-5">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">3</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Register onsite or online.</p>

						<div class="row">
							<div class="col-md-12 boxed bg-secondary">
								<h4 class="mb-3">Onsite Registration</h4>

								<ol>
							        	<li>Bring the required documents to our <span class="font-bold">Main Office</span>. </li>
							        	<ul>
							        		<li>Early applicants may submit their <span class="font-bold">Junior High ID and Birth Certificate</span> as placeholder or temporary documents for enrollment. </li>
							        		<li>The required documents must be submitted by <span class="font-bold">August 2021</span>.</li>
							        	</ul>
							        	<li class="mt-3">Accepted applications will be informed by their respective advisers through their contact numbers or through Facebook Messenger.</li>
							        </ol>

						
							</div>
							<div class="col-md-12 boxed bg-secondary">
								<h4 class="mb-3">Online Registration</h4>
							

								<ol>
						        	<li>Fill in the correct Branch Enrollment Form.</li>
						        	<ul>
						        		<li><a href="http://bit.ly/ICIILIGANOnlineReg2021" target="_blank">Iligan</a></li>
						        		<li><a href="http://bit.ly/ICICDOOnlineReg2021" target="_blank">CDO</a></li>
						        		<li><a href="http://bit.ly/ICIKapataganSHSEnrollmentForm" target="_blank">Kapatagan</a></li>
						        	</ul>
						        	<li class="mt-3">Our Marketing Team will contact you to confirm your enrollment.</li>
						        	<li class="mt-3">Once confirmed, please be ready with the required documents.</li>
						        	<li class="mt-3">Students residing in the city proper must submit their documents in the office. For students living outside the city proper, our marketing team will pick up your documents.</li>
						        </ol>
							</div>

							<div class="col-md-12 boxed bg-secondary">
								<h4 class="mb-3">Special Cases</h4>

								<p>Students with limited internet access may submit clear photos of their Junior High ID and Birth Certificate via Facebook Messenger through <a href="https://www.facebook.com/myici" target="_blank">@myici</a>. Students may also ask their friends to provide their enrollment details to us via Facebook or walk-in.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row pt-4">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">4</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4"><span class="font-bold">Wait for your enrollment confirmation.</span> Accepted applications will be informed by their respective advisers through their contact numbers or through Facebook Messenger.</p>
					</div>
				</div>
			</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					<h3 class=" mb-3">Old Students</h3>

					<div class="row pt-4">
						<div class="col-md-1">
							<div class="text-center pr-3">
								<p class="mb-0 c-primary">STEP</p>
								<a class="btn-social bg-primary m-0">1</a>
							</div>
						</div>
						<div class="col-md-11">
							<p class="pt-4">Pay all of your remaining balances at the <span class="font-bold">cashier</span>.</p>
						</div>
					</div>

					<div class="row pt-4">
						<div class="col-md-1">
							<div class="text-center pr-3">
								<p class="mb-0 c-primary">STEP</p>
								<a class="btn-social bg-primary m-0">2</a>
							</div>
						</div>
						<div class="col-md-11">
							<p class="pt-4">Once your balance is cleared, you may inform your adviser. </p>
						</div>
					</div>

					<div class="row pt-4">
						<div class="col-md-1">
							<div class="text-center pr-3">
								<p class="mb-0 c-primary">STEP</p>
								<a class="btn-social bg-primary m-0">3</a>
							</div>
						</div>
						<div class="col-md-11">
							<p class="pt-4">The Guidance Office also requires students to finish their teacher evaluations before the end of the school year. </p>
						</div>
					</div>

					<div class="row pt-4">
						<div class="col-md-1">
							<div class="text-center pr-3">
								<p class="mb-0 c-primary">STEP</p>
								<a class="btn-social bg-primary m-0">4</a>
							</div>
						</div>
						<div class="col-md-11">
							<p class="pt-4">Grade 11 Advisers will note which students to enroll for the next school year. Be sure to inform your advisers once you have your clearance.</p>
						</div>
					</div>

					<div class="row pt-4">
						<div class="col-md-1">
							<div class="text-center pr-3">
								<p class="mb-0 c-primary">STEP</p>
								<a class="btn-social bg-primary m-0">5</a>
							</div>
						</div>
						<div class="col-md-11">
							<p class="pt-4">You are now officially enrolled! Kindly look out for further announcements for the upcoming school year through our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>.</p>
						</div>
					</div>

			        <h3 class="mt-5 mb-3">Transferees</h3>

			        <div class="row">
			        <div class="col-md-6 pl-0">
				        <h4 class="mb-3 c-primary font-semibold">Requirements for Enrollment:</h4>
				        <p class="mb-0">Grade 11 Card</p>
				        <p class="mb-0">Certificate of Good Moral Character</p>
				        <p class="mb-0">NSO or SPA Birth Certificate</p>
				        <p class="mb-0">Form 137</p>
				    </div>

			        <div class="col-md-6">
						<h4 class="mb-3 c-primary font-semibold">On Site Registration</h4>
				        <ol>
				        	<li>Submit the needed documents to the Registrar. </li>
				        	<li class="mt-3">Fill in the <span class="font-bold">Registration Form</span>.</li>
				        	<li class="mt-3">Pay a downpayment of <span class="font-bold">P2750</span> to the Cashier. </li>
				        	<li class="mt-3">You are now officially enrolled! Kindly look out for further announcements for the upcoming school year through our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </li>
				        </ol>
					    </div>
					</div>
				</div>
			</div>


		<!-- <div id="tesda" class="mt-5">
			<h1 class="mb-4">TESDA Admission</h1>

			<p class=""><span class="c-magenta font-bold">PLEASE NOTE:</span> We currently accept walk-in applicants ONLY for <span class="font-bold">regular students</span>.</p>

				<div class="row">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">1</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Prepare the necessary documents.</p>

						<div class="d-flex">
							<div class="border border-dark">
								<div class="p-3 bg-primary text-white">
								 	<h6 class="c-white mb-0">High School Graduates</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">Form 138</p>
									<p class="mb-0">NCAE Result</p>
								</div>
							</div>
							<div class="border border-dark border-right-0 border-left-0">
								<div class="p-3 bg-secondary-2 text-white">
								 	<h6 class="c-white mb-0">College Level/Graduates</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">Transcript of Records</p>
									<p class="mb-0">Honorable Dismissal</p>
								</div>
							</div>
							<div class="border border-dark">
								<div class="p-3 bg-secondary-3 text-white">
								 	<h6 class="c-white mb-0">Transferees</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">True copy of Grades/Transcript of Records</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row pt-5">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">2</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Bring the required documents to our office.</p>
					</div>
				</div>

				<div class="row pt-3">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">3</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Fill up the Registration Form and pay the Registration Fee of <span class="font-bold">P950</span>.</p>
					</div>
				</div>

				<div class="row pt-3">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">4</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Take the entrance exam.</p>

						<ol>
							<li>For High School/Senior High School Graduates, you are required to take our Entrance Exam.</li>
							<ul>
								<li>Entrance Exam Fee: P50.</li>
							</ul>
							<li>For college graduates and college level transferees, you are not required to take the Entrance Exam.</li>
						</ol>
					</div>
				</div>

				<div class="row pt-1">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">5</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">You are now enrolled! Kindly wait for our frontline to contact you regarding your COR. </p>
					</div>
				</div>
		</div>


		<div id="faqs" class="mt-5">
			<h1 class="mb-4">Enrollment FAQs</h1>

			<div class="accordion mb-5" id="accordionExample">
				  <div class="no-border">
				    <div class="accord-header" id="headingOne">
				        <a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          What are your office hours?
				        </a>
				    </div>

				    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				      <div class="card-body ml-3">
				        <p>Our offices are open from 8:00 AM to 5:00 PM, Mondays to Fridays.</p>
				      </div>
				    </div>
				  </div>
				  <div class="no-border">
				    <div class="accord-header" id="headingTwo">
				        <a class=" collapsed"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				          Do you have STEM?
				        </a>
				    </div>
				    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				      <div class="card-body ml-3">
				        <p>We do not offer STEM as of the moment but we are looking into offering it in the near future. </p>
				        <p>We advise students to explore our current roster of SHS tracks as well, especially TVL if they are interested in STEM.</p>
				      </div>
				    </div>
				  </div>
				</div> -->

				<div class="card-body bg-secondary mt-5 p-4">
					<h4>Request Information</h4>
					<p class="mb-0">Have other concerns that we didn’t cover?</p>
					<p class="mb-0">Send us a direct message on our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </p>
				</div>

				
			</div>
			@include('frontend.includes.admissions-sidebar')
		</div>
	</div>
</section>

@endsection

@push('after-scripts')
<script>
function openCat(evt, cityName) {
  // Declare all variables
  var i, tabpagecontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabpagecontent = document.getElementsByClassName("tabpage-content");
  for (i = 0; i < tabpagecontent.length; i++) {
    tabpagecontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
@endpush