@extends('frontend.layouts.app')

@section('title', __('DepEd Scholarship'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/admissions/admissions-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Admissions</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif -->

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			<div class="col-md-10">
				<h1 class="mb-4">DepEd Scholarship</h1>

				<h4 class="mb-3 c-blue">Senior High School Voucher Program (SHS VP)</h4>

				<p>ICI is a recipient of the SHS VP provided by the Private Education Assistance Committee (PEAC) through DepEd.</p>
				<p>The program allocates financial assistance to participating private or non-DepEd public SHS schools. </p>

				<h4 class="mt-4 mb-3">Are you eligible for the program?</h4>

				<p>Incoming SHS students are classified into the following:</p>


			<div class="card-body bg-primary p-3">
		      	 <h5 class="c-white mb-0">Automatically Qualified Learners or Qualified Voucher Recipients (QVRs)</h5>
		    </div>

		      <div class="card-body bg-light pt-3 pb-0">
		      	<dl class="row">
				  <dt class="col-sm-3"><h6 class=" mb-0">Category A</h6></dt>
				  <dd class="col-sm-9">Grade 10 completers in DepEd public schools [SY 2020-2021]</dd>
				</dl>
		    </div>

		    <div class="card-body bg-secondary pt-2 pb-0">
		      	<dl class="row">
				  
				  <dt class="col-sm-3"><h6 class=" mb-0">Category B</h6></dt>
				  <dd class="col-sm-9">
				    <p class="mb-0">Grade 10 completers in state universities or colleges (SUCs) and local universities or colleges (LUCs) [SY 2020-2021]</p>
				  </dd>
				</dl>
		    </div>

		     <div class="card-body bg-light pb-0 mb-4">
		      	<dl class="row">
				  <dt class="col-sm-3"><h6 class=" mb-0">Category C</h6></dt>
				  <dd class="col-sm-9">
				    <p class="mb-0">Grade 10 completers who are Education Service Contracting (ESC) grantees [SY 2020-2021]</p>
				  </dd>
				</dl>
		    </div>

			<div class="card-body bg-secondary-2 p-3">
		      	 <h5 class="c-white mb-0">Voucher Applicants or Students who need to apply for the program</h5>
		    </div>

			<div class="card-body bg-light pt-3 pb-0">
		      	<dl class="row">
				  <dt class="col-sm-3"><h6 class=" mb-0">Category D</h6></dt>
				  <dd class="col-sm-9">Grade 10 completers in private schools who are not ESC grantees [SY 2020-2021]</dd>
				</dl>
		    </div>

		    <div class="card-body bg-secondary pt-2 pb-0">
		      	<dl class="row">
				  
				  <dt class="col-sm-3"><h6 class=" mb-0">Category E</h6></dt>
				  <dd class="col-sm-9">
				    <p class="mb-0">Grade 10 completers who completed Grade 10 prior to SY 2020- 2021 but not earlier than 2016 and had not previously enrolled for Grade 11</p>
				  </dd>
				</dl>
		    </div>

		    <div class="card-body bg-light pt-2 pb-0">
		      	<dl class="row">
				  
				  <dt class="col-sm-3"><h6 class=" mb-0">Category F</h6></dt>
				  <dd class="col-sm-9">
				    <p class="mb-0">Learners who had passed the ALS A&E Test for Grade 10 not earlier than 2016 and had not previously enrolled for Grade 11 or will take the ALS A&E Test in SY 2021- 2022<span class="c-magenta">*</span></p>
				  </dd>
				</dl>
		    </div>

		     <div class="card-body bg-secondary pt-2 pb-0 mb-4">
		      	<dl class="row">
				  
				  <dt class="col-sm-3"><h6 class=" mb-0">Category G</h6></dt>
				  <dd class="col-sm-9">
				    <p class="mb-0">Learners who passed the PEPT for Grade 10 not earlier than 2016 and had not previously enrolled for Grade 11 or will take the PEPT in SY 2021- 2022<span class="c-magenta">*</span></p>
				  </dd>
				</dl>
		    </div>

			

			<div class="card-body bg-secondary-3 p-3">
		      	 <h5 class="c-white mb-0">Not eligible</h5>
		    </div>

			<div class="card-body bg-light pt-3 pb-0 mb-0">
		      	<p class="col-sm-12 pb-2 mb-0">Learners who graduated High School in 2015 or earlier</p>
		    </div>

		    <div class="card-body bg-secondary pt-3 pb-0 mb-0">
		      	<p class="col-sm-12 pb-2 mb-0">Incoming Grade 12 learners who were not part of SHS VP in Grade 11</p>
		    </div>

		    <div class="card-body bg-light pt-3 pb-0 mb-0">
		      	<p class="col-sm-12 pb-2 mb-4">Non-Filipino learners</p>
		    </div>

			<p><span class="c-magenta">*</span>VAs may apply for the voucher while waiting for results of the ALS A&E Test and PEPT.</p>

			<p><em>Source: <span class="font-bold">DepEd ORDER No. 016 s. 2020</span></em></p>

			<div class="card-body bg-secondary mt-5 p-4">
				<h4>Request Information</h4>
				<p class="mb-0">Have other concerns that we didn’t cover?</p>
				<p class="mb-0">Send us a direct message on our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </p>
			</div>
			</div>
			@include('frontend.includes.admissions-sidebar')
		</div>
	</div>
</section>
@endsection

@push('after-scripts')
<script>
function openCat(evt, cityName) {
  // Declare all variables
  var i, tabpagecontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabpagecontent = document.getElementsByClassName("tabpage-content");
  for (i = 0; i < tabpagecontent.length; i++) {
    tabpagecontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
@endpush