@extends('frontend.layouts.app')

@section('title', __('TESDA Admissions'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/admissions/admissions-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Admissions</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif -->

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10">
				<h1 class="mb-4">TESDA Admissions</h1>

				<p class=""><span class="c-magenta font-bold">PLEASE NOTE:</span> We currently accept walk-in applicants ONLY for <span class="font-bold">regular students</span>.</p>

				<div class="row">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">1</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Prepare the necessary documents.</p>

						<div class="d-flex">
							<div class="border border-dark">
								<div class="p-3 bg-primary text-white">
								 	<h6 class="c-white mb-0">High School Graduates</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">Form 138</p>
									<p class="mb-0">NCAE Result</p>
								</div>
							</div>
							<div class="border border-dark border-right-0 border-left-0">
								<div class="p-3 bg-secondary-2 text-white">
								 	<h6 class="c-white mb-0">College Level/Graduates</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">Transcript of Records</p>
									<p class="mb-0">Honorable Dismissal</p>
								</div>
							</div>
							<div class="border border-dark">
								<div class="p-3 bg-secondary-3 text-white">
								 	<h6 class="c-white mb-0">Transferees</h6>
								</div>
								<div class="p-3">
									<p class="mb-0">Birth Certificate</p>
									<p class="mb-0">Certificate of Good Moral Character</p>
									<p class="mb-0">True copy of Grades/Transcript of Records</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row pt-5">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">2</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Bring the required documents to our office.</p>
					</div>
				</div>

				<div class="row pt-3">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">3</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Fill up the Registration Form and pay the Registration Fee of <span class="font-bold">P950</span>.</p>
					</div>
				</div>

				<div class="row pt-3">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">4</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">Take the entrance exam.</p>

						<ol>
							<li>For High School/Senior High School Graduates, you are required to take our Entrance Exam.</li>
							<ul>
								<li>Entrance Exam Fee: P50.</li>
							</ul>
							<li>For college graduates and college level transferees, you are not required to take the Entrance Exam.</li>
						</ol>
					</div>
				</div>

				<div class="row pt-1">
					<div class="col-md-1">
						<div class="text-center pr-3">
							<p class="mb-0 c-primary">STEP</p>
							<a class="btn-social bg-primary m-0">5</a>
						</div>
					</div>
					<div class="col-md-11">
						<p class="pt-4">You are now enrolled! Kindly wait for our frontline to contact you regarding your COR. </p>
					</div>
				</div>

				<div class="card-body bg-secondary mt-5 p-4">
					<h4>Request Information</h4>
					<p class="mb-0">Have other concerns that we didn’t cover?</p>
					<p class="mb-0">Send us a direct message on our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </p>
				</div>
			</div>
			@include('frontend.includes.admissions-sidebar')
		</div>
	</div>
</section>
@endsection

@push('after-scripts')
<script>
function openCat(evt, cityName) {
  // Declare all variables
  var i, tabpagecontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabpagecontent = document.getElementsByClassName("tabpage-content");
  for (i = 0; i < tabpagecontent.length; i++) {
    tabpagecontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
@endpush