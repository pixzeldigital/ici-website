@extends('frontend.layouts.app')

@section('title', __('Senior High School Admissions'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/admissions/admissions-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Admissions</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif -->

<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10 mt-3">
        <div class="row">
          <div class="col-lg-9 col-md-12 pl-0">
            <h1 class="mb-4">Crash Courses</h1>
          </div>
          <div class="col-lg-3 col-md-12 mb-5 pl-0">
            <a href="/education/short-courses" class="btn btn-primary text-right">View Crash Courses</a>
          </div>
        </div>
        
				

				<ol>
					<li>We have no prior requirements needed to enroll in our crash courses.</li>
					<li class="mt-3">Students may enroll anytime but classes will start depending on the availability of the instructor. Instructors will have the discretion to choose the schedule of the class.</li>
					<li class="mt-3">Crash courses normally have 2-3 sessions per week.</li>
					<li class="mt-3">Payment Terms & Fees </li>
					<ul>
						<li>If fully paid upon enrollment (Cash only), the student will receive a 10% discount. </li>
						<li><span class="font-bold">Certificate of Completion Fee:</span> P50 (Required except for NCII courses)</li>
						<li><span class="font-bold">Authentication Stamp Fee:</span> P10/page (Required except for NCII courses)</li>
					</ul>
				</ol>

				<h6 class="mt-4">NOTICE</h6>

				<p>Crash courses may not push through if we do not reach the minimum number of students required.</p>

				<p>Interested students may enlist through walk-in at our offices or through Facebook Messenger at <a href="https://www.facebook.com/myici" target="_blank">@myici</a>. Send us your full name, contact number/s, and preferred course. </p>

				<p>Enrolment for your course will immediately start once we reach the quota. Our frontlines will be informing you through the number you provided. Please make sure to provide a valid number. </p>

        <div class="card-body bg-secondary mt-5 p-4">
          <h4>Request Information</h4>
          <p class="mb-0">Have other concerns that we didn’t cover?</p>
          <p class="mb-0">Send us a direct message on our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </p>
        </div>
			</div>
			@include('frontend.includes.admissions-sidebar')
		</div>
	</div>
</section>
@endsection

@push('after-scripts')
<script>
function openCat(evt, cityName) {
  // Declare all variables
  var i, tabpagecontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabpagecontent = document.getElementsByClassName("tabpage-content");
  for (i = 0; i < tabpagecontent.length; i++) {
    tabpagecontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
@endpush