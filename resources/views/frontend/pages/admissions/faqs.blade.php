@extends('frontend.layouts.app')

@section('title', __('Enrollment FAQs'))

@push('after-styles')
<link href="{{ asset('css/flickity.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/home.css') }}?v={{ uniqid() }}" rel="stylesheet"> -->
@endpush


@section('content')
<section class="imagebg height-30" id="home-banner">
  <div class="background-image-holder">
    <img src="{{asset('img/admissions/admissions-hero.jpg')}}">
  </div>
  <div class="container pos-vertical-center">
    <div class="row">
      <div class="col-md-7">
        <h1 class="title">Admissions</h1>
      </div>
    </div>
  </div>
</section>

<!-- @if (config('boilerplate.frontend_breadcrumbs'))
    @include('frontend.includes.partials.breadcrumbs')
@endif -->


<section class="space--xs">
	<div class="container">
		<div class="row p-0">
			
			<div class="col-md-10">
				<h1 class="mb-4">Enrollment FAQs</h1>
				
				<div class="accordion mb-5" id="accordionExample">
				  <div class="no-border">
				    <div class="accord-header" id="headingOne">
				        <a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          What are your office hours?
				        </a>
				    </div>

				    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				      <div class="card-body ml-3">
				        <p>Our offices are open from 8:00 AM to 5:00 PM, Mondays to Fridays.</p>
				      </div>
				    </div>
				  </div>
				  <div class="no-border">
				    <div class="accord-header" id="headingTwo">
				        <a class=" collapsed"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				          Do you have STEM?
				        </a>
				    </div>
				    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				      <div class="card-body ml-3">
				        <p>a. For now, only our Iligan Branch has STEM. We are looking into offering STEM for the rest of our branches in the near future. </p>
				        <p>b. We advise students to explore our current roster of SHS tracks, particularly TVL, if they are interested in STEM. See our <a href="/education/shs">SHS Offerings</a>.</p>
				      </div>
				    </div>
				  </div>
				  <div class="no-border">
				    <div class="accord-header" id="headingTwo">
				        <a class=" collapsed"  data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
				          How can I enroll in your TESDA courses?
				        </a>
				    </div>
				    <div id="collapse3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				      <div class="card-body ml-3">
				        <p>a. For SY 2021-2022, we will not be accepting walk-in applications for our TESDA Programs. </p>
				        <p>b. We will only accept applicants with legitimate TESDA scholarships. To apply, email <a href="mailto:registrar@ici.edu.ph">registrar@ici.edu.ph</a>.</p>
				      </div>
				    </div>
				  </div>
				</div>

				<div class="card-body bg-secondary mt-5 p-4">
					<h4>Request Information</h4>
					<p class="mb-0">Have other concerns that we didn’t cover?</p>
					<p class="mb-0">Send us a direct message on our <a href="https://www.facebook.com/myici" target="_blank">Facebook Page</a>. </p>
				</div>
				
			</div>
			@include('frontend.includes.admissions-sidebar')
		</div>
	</div>
</section>
@endsection