    // start banner
    var duration = 9000,
        // currentindex = Math.round(Math.random()*(2-1)+1),
        currentindex = 1,
        totalitems = $('.info').length,
        timeline = new TimelineMax(),
        text,
        item,
        bar;

    function runslide() {

        item = $('[data-id='+ currentindex +']'),
        bar = item.find('.bar'),
        text = item.find('slide-text');

        item.addClass('active');

        timeline.play();
        timeline
            .to(bar, 0, {left: '-100%'})
            .to(bar, duration, {left: '0%', ease: Linear.easeNone, delay: .75, onComplete: function(){
                item.addClass('fadeout');
        }})
          .to(text, 1.5, {
              x: 100,
              opacity: 0,
            }, {
              x: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, '-=0.6')
          .to(bar, duration, {left: '100%', ease: Power4.easeIn, delay: .25, onComplete: function(){
              if(currentindex == totalitems){
                  currentindex = 1;
              } else {
                  currentindex++;
              }

              item.removeClass('active fadeout');
              timeline.clear();
              window.runslide();
          }});
    }

    function clickslide(e) {

        currentindex = e;
        timeline.clear();
        TweenMax.to(bar, duration, {left: '0%', ease: Power4.easeOut});
        TweenMax.to(bar, duration, {left: '100%', ease: Power4.easeIn, delay: duration/10});

        $('.photo, .info').removeClass('active fadeout');
        window.runslide();


    }

    $(function(){
      
      $('.banner')
          .on('click', '.info:not(.active, .fadeout)', function(){
            clickslide($(this).attr('data-id'));
            timeline.pause();
          })
          .on('mouseover', '.info.active:not(.fadeout)', function(){
            timeline.pause();
          })
          .on('mouseout', '.info.active:not(.fadeout)', function(){
            timeline.play();
          });


      $('.photo').each(function(){
          $(this).css({'background-image': 'url('+ $(this).attr('data-image') +')'});
      });
      // end of slider


      // horizontal scroll
      gsap.registerPlugin(ScrollTrigger);

      let sections = gsap.utils.toArray(".panel");

      gsap.to(sections, {
          xPercent: -100 * (sections.length - 1),
          ease: "none",
          scrollTrigger: {
            trigger: ".h-scroll-panel",
            pin: true,
            scrub: true,
            // snap: 1 / (sections.length - 1),
            // base vertical scrolling on how wide the container is so it feels more natural.
            end: () => "+=" + document.querySelector(".scroll-section").offsetWidth
          }
      });
     
      gsap.from("progress", {
          scrollTrigger: {
              trigger: ".main-container",
              // scroller: "progress",
              scrub: 0.3,
              start: "top bottom",
              end: () => "+=" + document.querySelector(".main-container").offsetHeight
          },
          scaleY: 0,
          toggleActions: "reverse",
          transformOrigin: "top bottom", 
          ease: "none"
      });

    })
    var slider = new TuinSlider('#slider', {
      onLeave: function(index) {
        // Tween timeline object
        var tl = new TimelineLite();

        // Select elements in current slide, 
        // if not the animation triggers for all elements
        var items = document.querySelectorAll('.slider__item')
        var item = items[index];

        // Animate title
        var title = item.querySelectorAll('.title');

        // First slide = 0
        var sigma = item.querySelector('.sigma');

        // Second slide = 1
        var aiteo = item.querySelector('.aiteo');

        // Third slide = 2
        var indigo = item.querySelectorAll('.indigo');

        // Third slide = 2
        var fumman = item.querySelectorAll('.fumman');

        // Exit animations
        if (index === 0) {
          tl.staggerTo(title, 0.8, {
              y: -100,
              opacity: 0,
              ease: Power3.easeOut

            }, 0.5)
            .to(sigma, 0.6, {
              x: 200,
              opacity: 0,
              ease: Power3.easeOut

            });

        }

        if (index === 1) {
          tl.staggerTo(title, 0.8, {
              y: 100,
              opacity: 0,
              ease: Power3.easeOut
            }, 0.5)
            .to(aiteo, 0.5, {
              y: -100,
              opacity: 0,
              ease: Power3.easeOut
            }, '=-1');

        }

        if (index === 2) {
          tl.staggerTo(title, 0.8, {
              y: -100,
              opacity: 0,
              ease: Power3.easeOut
            }, 0.5)
            .to(indigo, 0.5, {
              x: 100,
              opacity: 0,
              ease: Power3.easeOut
            });
        }
        if (index === 3) {
          tl.staggerTo(title, 0.8, {
              y: -100,
              opacity: 0,
              ease: Power3.easeOut
            }, 0.5)
            .to(fumman, 0.5, {
              x: 100,
              opacity: 0,
              ease: Power3.easeOut
            });

        }

        return tl;

      },

      afterLoad: function(index) {
        // Tween timeline object
        var tl = new TimelineLite();

        // Select elements in current slide, 
        // if not the animation triggers for all elements
        var items = document.querySelectorAll('.slider__item')
        var item = items[index];

        // Animate title every slide item
        var title = item.querySelectorAll('.title');

        // Hide the content case click in between values dots
        // By example you are slide one and click slide third
        // this make hide the content in slide two
        var content = item.querySelector('.slider__content');
        TweenLite.set(content, {
          visibility: 'visible'
        });

        // First slide = 0
        var sigma = item.querySelector('.sigma');

        // Second slide = 1
        var aiteo = item.querySelector('.aiteo');

        // Third slide = 2
        var indigo = item.querySelectorAll('.indigo');

         // Third slide = 2
        var fumman = item.querySelectorAll('.fumman');

        // Enter animations
        if (index === 0) {
          tl.staggerFromTo(title, 1, {
              y: 100,
              opacity: 0,
            }, {
              y: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, 0.5)
            .fromTo(sigma, 1.5, {
              x: 100,
              opacity: 0,
            }, {
              x: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, '-=0.6');

        }

        if (index === 1) {
          tl
            .staggerFromTo(title, 0.8, {
              y: -100,
              opacity: 0,
            }, {
              y: 0,
              opacity: 1,
              ease: Power3.easeOut
            }, 0.5).fromTo(aiteo, 0.5, {
              y: 100,
              opacity: 0
            }, {
              y: 0,
              opacity: 1
            }, '=-1');

        }

        if (index === 2) {
          tl.staggerFromTo(title, 0.8, {
              y: 100,
              opacity: 0,
            }, {
              y: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, 0.5)
            .staggerFromTo(indigo, 1.5, {
              x: 100,
              opacity: 0,
            }, {
              x: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, 0.5);

        }

        if (index === 3) {
          tl.staggerFromTo(title, 0.8, {
              y: 100,
              opacity: 0,
            }, {
              y: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, 0.5)
            .staggerFromTo(fumman, 1.5, {
              x: 100,
              opacity: 0,
            }, {
              x: 0,
              opacity: 1,
              ease: Power3.easeOut

            }, 0.5);

        }
        return tl;
      }
    });


                
 
