<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\TermsController;
use Tabuna\Breadcrumbs\Trail;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('frontend.index'));
    });

Route::get('/contact-us', [HomeController::class, 'contact'])
    ->name('pages.contact')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')->push(__('Contact Us'), route('frontend.pages.contact'));
    });

Route::get('/careers', [HomeController::class, 'careers'])
    ->name('pages.careers')
    ->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('Careers'), route('frontend.pages.careers'));
    });

Route::get('/calendar', [HomeController::class, 'calendar'])
    ->name('pages.calendar')
    ->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('Calendar'), route('frontend.pages.calendar'));
    });

//about

Route::get('/about', [HomeController::class, 'about'])
    ->name('pages.about')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')->push(__('About'), route('frontend.pages.about'));
    });

Route::get('/about/leadership', [HomeController::class, 'leadership'])
->name('pages.leadership')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.about')->push(__('Leadership'), route('frontend.pages.leadership'));
});

Route::get('/about/faculty-staff', [HomeController::class, 'faculty_staff'])
->name('pages.faculty-staff')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.about')->push(__('Faculty & Staff'), route('frontend.pages.faculty-staff'));
});

Route::get('/gallery', [HomeController::class, 'gallery'])
->name('pages.gallery')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.about')->push(__('Gallery'), route('frontend.pages.gallery'));
});

Route::get('terms', [TermsController::class, 'index'])
    ->name('pages.terms')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('frontend.index')
            ->push(__('Terms & Conditions'), route('frontend.pages.terms'));
    });

//admissions

Route::get('/admissions', [HomeController::class, 'admissions'])
->name('pages.admissions.index')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('Admissions'), route('frontend.pages.admissions.index'));
});

Route::get('/admissions/crash-courses', [HomeController::class, 'crash_courses'])
->name('pages.admissions.crash-courses')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.admissions.index')->push(__('Crash Courses'), route('frontend.pages.admissions.crash-courses'));
});

Route::get('/admissions/faqs', [HomeController::class, 'faqs'])
->name('pages.admissions.faqs')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.admissions.index')->push(__('Enrollment FAQs'), route('frontend.pages.admissions.faqs'));
});

Route::get('/admissions/scholarship', [HomeController::class, 'scholarship'])
->name('pages.admissions.scholarship')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.admissions.index')->push(__('DepEd Scholarship'), route('frontend.pages.admissions.scholarship'));
});

Route::get('/admissions/tesda', [HomeController::class, 'tesda'])
->name('pages.admissions.tesda')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.admissions.index')->push(__('TESDA Admissions'), route('frontend.pages.admissions.tesda'));
});

//education

Route::get('/education', [HomeController::class, 'education'])
->name('pages.education.index')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('Education'), route('frontend.pages.education.index'));
});

Route::get('/education/short-courses', [HomeController::class, 'short_courses'])
->name('pages.education.short-courses')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.education.index')->push(__('short-courses'), route('frontend.pages.education.short-courses'));
});

Route::get('/education/shs', [HomeController::class, 'shs_educ'])
->name('pages.education.shs')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.education.index')->push(__('shs'), route('frontend.pages.education.shs'));
});

Route::get('/education/tesda', [HomeController::class, 'tesda_educ'])
->name('pages.education.tesda')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.education.index')->push(__('tesda'), route('frontend.pages.education.tesda'));
});


//student-life

Route::get('/student-life', [HomeController::class, 'student_life'])
->name('pages.student-life.index')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('Student Life'), route('frontend.pages.student-life.index'));
});

Route::get('/student-life/holistic-growth', [HomeController::class, 'holistic_growth'])
->name('pages.student-life.holistic-growth')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.student-life.index')->push(__('Holistic Growth'), route('frontend.pages.student-life.holistic-growth'));
});

Route::get('/student-life/extracurriculars', [HomeController::class, 'extracurriculars'])
->name('pages.student-life.extracurriculars')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.student-life.index')->push(__('Extracurriculars for Students'), route('frontend.pages.student-life.extracurriculars'));
});

Route::get('/student-life/atici', [HomeController::class, 'atici'])
->name('pages.student-life.atici')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.pages.student-life.index')->push(__('Student Life at ICI'), route('frontend.pages.student-life.atici'));
});

//news

Route::get('/news', [HomeController::class, 'news'])
->name('pages.news.index')
->breadcrumbs(function (Trail $trail) {
    $trail->parent('frontend.index')->push(__('News'), route('frontend.pages.news.index'));
});