<?php

namespace App\Http\Controllers\Backend;

use Image;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::latest();

        if($request->keyword) {
            $news = $news->where('title', 'LIKE', '%' . $request->keyword . '%');
        }

        $news = $news->paginate(5);
        return view('backend.modules.news.index', compact(
            'news'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'image' => 'required',
            'date_created' => 'required',
            'summary' => 'required|max:200',
            'content' => 'required'
        ]);

        $news = new News;
        $news->fill($request->all());
        $news->date_created = date('Y-m-d', strtotime($request->date_created));
        $news->created_by = auth()->user()->id;
        $news->save();

        if ($request->has('image')) {
            $image = $request->file('image');
            $destinationPath = public_path(). '/uploads/news';
            $img = Image::make($image);
            $filename = $request->slug. '.jpg';
            $img->save($destinationPath.'/'.$filename);

            $news->image = $filename;
            $news->save();
        }

        return redirect()->route('admin.news.index')
                        ->with('success', 'News Article created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);

        return view('backend.modules.news.show', compact(
            'news'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);

        return view('backend.modules.news.edit', compact(
            'news'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'date_created' => 'required',
            'summary' => 'required|max:200',
            'content' => 'required'
        ]);

        $news = News::where('id', $id)->first();
        $news->fill($request->all());
        $news->save();

        return redirect()->route('admin.news.index')
                        ->with('success', 'News Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->update([
            'is_deleted' => 1,
            'deleted_by' => auth()->user()->id,
            'deleted_at' => now()
        ]);

         return redirect()->route('admin.news.index')
                        ->with('deleted', 'News Article deleted');
    }
}
