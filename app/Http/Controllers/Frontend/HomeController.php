<?php

namespace App\Http\Controllers\Frontend;

/**
 * Class HomeController.
 */
class HomeController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function contact()
    {
        return view('frontend.contact-us');
    }

    public function careers()
    {
        return view('frontend.careers');
    }

    public function calendar()
    {
        return view('frontend.calendar');
    }

    // about pages

    public function about()
    {
        return view('frontend.pages.about.index');
    }

    public function leadership()
    {
        return view('frontend.pages.about.leadership');
    }

    public function faculty_staff()
    {
        return view('frontend.pages.about.faculty-staff');
    }

    public function gallery()
    {
        return view('frontend.pages.gallery');
    }

    // admissions

    public function admissions()
    {
        return view('frontend.pages.admissions.index');
    }

    public function crash_courses()
    {
        return view('frontend.pages.admissions.crash-courses');
    }

    public function faqs()
    {
        return view('frontend.pages.admissions.faqs');
    }

    public function scholarship()
    {
        return view('frontend.pages.admissions.scholarship');
    }

    public function tesda()
    {
        return view('frontend.pages.admissions.tesda');
    }


    //education

    public function education()
    {
        return view('frontend.pages.education.index');
    }

    public function short_courses()
    {
        return view('frontend.pages.education.short-courses');
    }

    public function shs_educ()
    {
        return view('frontend.pages.education.shs');
    }

    public function tesda_educ()
    {
        return view('frontend.pages.education.tesda');
    }

    //student-life

    public function student_life()
    {
        return view('frontend.pages.student-life.index');
    }

    public function holistic_growth()
    {
        return view('frontend.pages.student-life.holistic-growth');
    }

    public function extracurriculars()
    {
        return view('frontend.pages.student-life.extracurriculars');
    }

    public function atici()
    {
        return view('frontend.pages.student-life.atici');
    }

    //news

    public function news()
    {
        return view('frontend.pages.news.index');
    }
}
