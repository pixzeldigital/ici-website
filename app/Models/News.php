<?php

namespace App\Models;

use App\Domains\Auth\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class News extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'news';

    protected $guarded = [];

    protected $appends = [
        'author',
        'category',
    ];

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function getAuthorAttribute() {
        return User::where('id', $this->created_by)
                    ->pluck('name')
                    ->first();
    }

    public function getCategoryAttribute() {
        return Category::where('id', $this->category_id)
                    ->pluck('name')
                    ->first();
    }
}
